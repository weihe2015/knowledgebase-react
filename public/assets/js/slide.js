var tpj=jQuery;					
var revapi48;
tpj(document).ready(function() {
	$("a").filter(function() {
		return this.hostname && this.hostname !== location.hostname;
	}).attr('target', '_blank');
	if(tpj("#slider").revolution == undefined){
		revslider_showDoubleJqueryError("#slider");
	}else{
		revapi48 = tpj("#slider").show().revolution({
			sliderType:"standard",
			jsFileLocation:"assets/revolution/js/",
			sliderLayout:"fullscreen",
			dottedOverlay:"none",
			delay:9000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				arrows: {
					style: "gyges",
					enable: false,
					hide_onmobile: false,
					hide_onleave: false,
					tmp: '',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 10,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 10,
						v_offset: 0
					}
				},
				bullets: {
					enable: false,
					hide_onmobile: false,
					style: "gyges",
					hide_onleave: false,
					direction: "horizontal",
					h_align: "center",
					v_align: "_top",
					h_offset: 0,
					v_offset: 20,
					space: 5,
					tmp: ''
				}
			},
			parallax: {
				type:"mouse",
				origo:"slidercenter",
				speed:2000,
				levels:[2,3,4,5,6,7,12,16,10,50],
			},
			responsiveLevels:[1240,1024,778,480],
			gridwidth:[1240,1024,778,480],
			gridheight:[868,768,960,720],
			lazyType:"none",
			shadow:0,
			spinner:"off",
			stopLoop:"on",
			stopAfterLoops:0,
			stopAtSlide:1,
			shuffle:"off",
			autoHeight:"off",
			fullScreenAlignForce:"off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "",
			disableProgressBar:"on",
			hideThumbsOnMobile:"off",
			hideSliderAtLimit:0,
			hideCaptionAtLimit:0,
			hideAllCaptionAtLilmit:0,
			debugMode:false,
			fallbacks: {
				simplifyAll:"off",
				nextSlideOnWindowFocus:"off",
				disableFocusListener:false,
			}
		});

		revapi48.bind("revolution.slide.onchange",function (e,data) {
			if($('.top-head').hasClass('transparent') || $('.top-head').hasClass('boxed-transparent')){
				if($('#slider ul > li').eq(data.slideIndex-1).hasClass('dark') ){
					$('.top-head').removeClass('not-dark');
					$('.top-head').addClass('dark');
					var logo = $('.logo').find('img').attr('src').replace("logo.png", "logo-light.png");
					$('.logo').find('img').attr('src',logo);
				} else {
					$('.top-head').removeClass('dark');
					$('.top-head').addClass('not-dark');
					var logo = $('.logo').find('img').attr('src').replace("logo-light.png", "logo.png");
					$('.logo').find('img').attr('src',logo);
				}
				if($('.top-head').hasClass('sticky-nav')){
					var logo = $('.logo').find('img').attr('src').replace("logo-light.png", "logo.png");
					$('.logo').find('img').attr('src',logo);
				}
			}
		});
	}
});	/*ready*/