import { USER_LOGGED_IN, USER_OAUTH_URL_FETCH, USER_LOGGED_OUT, USER_OAUTH_ENDSESSION_URL_FETCH,
	USER_INFO_FETCHED, USER_INFO_CHANGED, USERS_INFO_FETCHED, USER_DELETED } from '../types';

export default function user(state={}, action={}) {
	switch(action.type) {
		case USER_LOGGED_IN:
			return action.user;
		case USER_OAUTH_URL_FETCH:
			return { ...state, oauthUrl: action.user }; 
		case USER_OAUTH_ENDSESSION_URL_FETCH:
			return { ...state, oauthEndSessionUrl: action.user };
		case USER_LOGGED_OUT:
			return {};
		case USER_INFO_FETCHED:
			return {...state, newUser: action.user};
		case USERS_INFO_FETCHED:
		case USER_INFO_CHANGED:
		case USER_DELETED:
		default:
			return state;
	}
}
