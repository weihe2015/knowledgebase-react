import { PRODUCT_FETCHED, PRODUCT_CREATED, 
  PRODUCT_MODIFIED, PRODUCT_DELETED } from '../types';

  export default function products(state={}, action={}) {
    switch(action.type) {
      case PRODUCT_FETCHED:
      case PRODUCT_MODIFIED:
        return { ...state, topicOptions: action.data.topicOptions, clientProductOptions: action.data.clientProductOptions };
      case PRODUCT_CREATED:
      case PRODUCT_DELETED:
      default:
        return state;
    }
  }