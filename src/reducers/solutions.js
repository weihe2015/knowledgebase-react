import { createSelector } from 'reselect';
import { SOLUTIONS_FETCH,
	   SOLUTIONS_SEARCH,
		 SOLUTION_CREATED,
		 SOLUTION_INC_VIEW_NUMBER,
		 SOLUTION_INC_RATINGUP,
		 SOLUTION_INC_RATINGDOWN,
		 SOLUTION_UPDATED,
		 SOLUTION_APPROVAL,
		 SOLUTION_REJECTION,
		 SOLUTION_DRAFT,
		 SOLUTION_UPDATED_BY_ATTACHMENT,
		 SOLUTION_GETBYID,
		 SOLUTION_FETCH_BY_CATEGORY,
		 SOLUTION_DOWNLOAD_ATTACHMENT,
	 	 NEW_SOLUTION_ID_FETCHED,
		 SOLUTION_DOWNLOAD_ALL_ATTACHMENTS_BY_SOLUTIONID,
		 SOLUTION_INSERT_IMAGE_TO_CONTENT,
		 SOLUTION_IMAGE_FETCH,
		 SOLUTION_HAS_USER_VOTED
	  } from '../types';

export default function solutions(state = {}, action = {}) {
	switch(action.type) {
		case SOLUTION_UPDATED_BY_ATTACHMENT:
		case SOLUTIONS_FETCH:
		case SOLUTIONS_SEARCH:
		case SOLUTION_CREATED:
		case SOLUTION_GETBYID:
		case SOLUTION_FETCH_BY_CATEGORY:
			return {...state, ...action.data.entities.solutions}
		case SOLUTION_UPDATED:
		case SOLUTION_APPROVAL:
		case SOLUTION_REJECTION:
		case SOLUTION_DRAFT:
		case SOLUTION_INC_VIEW_NUMBER:
		case SOLUTION_INC_RATINGUP:
		case SOLUTION_INC_RATINGDOWN:
		case SOLUTION_DOWNLOAD_ATTACHMENT:
			return {...state, ...action.solutions}
		case NEW_SOLUTION_ID_FETCHED:
		case SOLUTION_IMAGE_FETCH:
		case SOLUTION_HAS_USER_VOTED:
			return {...state, ...action.data}
		case SOLUTION_DOWNLOAD_ALL_ATTACHMENTS_BY_SOLUTIONID:
		case SOLUTION_INSERT_IMAGE_TO_CONTENT:
		default:
			return state;
	}
}

// Selectors:
export const solutionsSelector = state => state.solutions;

export const allSolutionsSelector = createSelector(
	solutionsSelector,
	solutionsHash => Object.values(solutionsHash)
);
