import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const AllUserRoute = ({ isConfirmed, isAuthenticated, component: Component, ...rest}) => (
	<Route {...rest} render={props => (isConfirmed && isAuthenticated) ? <Component {...props} /> : <Redirect to={{
		pathname: '/',
		state: { fromPath: props.location }
	}} />} />
);

AllUserRoute.propTypes = {
	component: PropTypes.func.isRequired,
	isConfirmed: PropTypes.bool.isRequired,
	isAuthenticated: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
	return {
		isConfirmed: !!state.user.confirmed,
		isAuthenticated: !!state.user.token
	}
}

export default connect(mapStateToProps)(AllUserRoute);