import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const AdminRoute = ({ isAuthenticated, component: Component, isAdmin, ...rest}) => (
	<Route {...rest} render={props => (isAuthenticated && isAdmin) ? <Component {...props} /> : <Redirect to={{
		pathname: '/dashboard',
		state: { fromPath: props.location }
		}}
	/>} />
);

AdminRoute.propTypes = {
	component: PropTypes.func.isRequired,
	isAuthenticated: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
	return {
		isAuthenticated: !!state.user.token,
		isAdmin: state.user.role === 'admin' || state.user.role === 'manager' || state.user.role === 'tech_senior'
	}
}

export default connect(mapStateToProps)(AdminRoute);
