import React from 'react';
import { Card, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const AddSolutionCta = () => (
	<Card centered>
		<Card.Content textAlign="center">
			<Card.Header>Add new Solution</Card.Header>
			<Link to="/solutions/new">
				<Icon name="plus circle" />
			</Link>
		</Card.Content>
	</Card>
)

export default AddSolutionCta;