import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import queryString from 'query-string';
import { Form, Button, Dimmer, Loader, Message } from '../../utils/reactSemanticUI';
import InlineError from '../messages/InlineError';
import Footer from '../navigation/Footer';
import { login } from '../../actions/auth';
import HomeHeader from '../navigation/HomeHeader';
import userManager from "../../utils/userManager";

export const TopBar = () => (
  <div className="top-bar main-bg">
    <div className="container">
      <div className="center-tbl">
        <ul className="top-info">
          <li><a href="mailto:support@translations.com?subject=TransPerfect |
						Knowledge Base Support request &body=Dear%20Support%20Team,%0A%0APlease%20provide%20assistance%20for%20the%20following:
						%0A%0A%0AApplication: KnowledgeBase%0ARequestor%27s%20Name:%0ARequestor%27s%20Email%20address:%0ASupport
						%20Issue%20%28please%20be%20specific%20and%20include%20screen%20shots%20as%20needed%29:%0A%0A%0AThank%20you," >
            <i className="fa fa-globe"></i>Technology Services Team</a></li>
        </ul>
        <ul>
          <li><Link to="#" data-toggle="modal" data-target="#disclaimermodal"><i className="fa fa-bookmark"></i>Disclaimer</Link></li>
          <li><Link to="#" data-toggle="modal" data-target="#toumodal"><i className="fa fa-legal"></i>Terms of Use</Link></li>
          <li><Link to="#" data-toggle="modal" data-target="#versionmodal"><i className="fa fa-legal"></i>About GlobalLink Knowledge Base</Link></li>
        </ul>
      </div>
    </div>
  </div>
);

const sliderStyle = {
  'display': 'none'
};

const tpCaptionSytle2 = {
  'zIndex': '7',
  'whiteSpace': 'nowrap'
};

export const VersionModel = () => (
  <div className="modal fade" id="versionmodal" tabIndex="-1" role="dialog" aria-labelledby="versionmodal">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 className="modal-title" id="gridSystemModalLabel">GlobalLink Knowledgebase System</h4>
        </div>
        <div className="modal-body">
          <div className="padding-horizontal-20 padding-vertical-20">
            <p>GlobalLink Technology Services Knowledge Base version 2.0</p>
            <p>March 2019</p>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
)

export const DisclaimModel = () => (
  <div className="modal fade" id="disclaimermodal" tabIndex="-1" role="dialog" aria-labelledby="disclaimermodal">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 className="modal-title" id="gridSystemModalLabel">Disclaimer</h4>
        </div>
        <div className="modal-body">
          <div className="padding-horizontal-20 padding-vertical-20">
            <p>The material in this website could include technical inaccuracies or other errors.
            The User's use and browsing of the website is at the User's risk.
            Neither TransPerfect nor any other party involved in creating, producing or
            delivering the website shall be liable for any direct, incidental, consequential,
            indirect or punitive damages arising out of the User's access to, or use of, the website.
            TransPerfect does not warrant that the functional aspects of the
            website will be uninterrupted or error free, or that this website or the server
            that makes it available are free of viruses or other harmful components.
            Please note that some jurisdictions may not allow the exclusion of implied warranties,
            so some of the above exclusions may not apply to particular Users.
								TransPerfect reserves the right to make changes to this website at any time without notice.</p>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
);

export const UseModel = () => (
  <div className="modal fade" id="toumodal" tabIndex="-1" role="dialog" aria-labelledby="toumodal">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 className="modal-title" id="gridSystemModalLabel">Terms of Use</h4>
        </div>
        <div className="modal-body">
          <div className="padding-horizontal-20 padding-vertical-20">
            <p>Confidential. The material on this page is intended solely for GlobalLink,
								their employees and customers that utilize TransPerfect services. Contents shall not be unlawfully distributed.</p>
            <p>&copy; {new Date().getFullYear()} TransPerfect</p>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
);

class HomePage extends React.Component {
  state = {
    data: {
      email: '',
      password: ''
    },
    loading: false,
    errors: {},
    warning: {},
    SSOWarning: {},
    success: false,
    redirectOAuth: false,
    redirectOAuthUrl: '',
  }

  onChange = e => {
    this.setState({
      data: {
        ...this.state.data,
        errors: {},
        warning: {},
        SSOWarning: {},
        [e.target.name]: e.target.value
      }
    });
  }

  onSubmit = (evt) => {
    evt.stopPropagation();
    evt.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });

    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      const { data } = this.state;
      this.props.login(data).then(res => {
        this.setState({ ...this.state, success: true, loading: false });
      }).catch(err => {
        const statusCode = err.response.status;
        if (statusCode === 401) {
          this.setState({
            warning: {},
            SSOWarning: {},
            errors: err.response.data.errors,
            loading: false
          });
        }
        else if (statusCode === 400 || statusCode === 404) {
          this.setState({
            warning: err.response.data.errors,
            errors: {},
            SSOWarning: {},
            loading: false
          });
        }
        else if (statusCode === 403) {
          this.setState({
            errors: {},
            warning: {},
            SSOWarning: err.response.data.errors,
            loading: false
          });
        }
      });
    }
  }

  handleLoginWithTPTAuth = (event) => {
    event.preventDefault();
    event.stopPropagation();
    userManager.signinRedirect();
  }

  validate = (data) => {
    const errors = {};
    if (!data.email) {
      errors.email = "Email/Username cannot be empty.";
    }
    if (!data.password) {
      errors.password = "Password cannot be empty.";
    }
    return errors;
  }

  render() {
    const { isAuthenticated } = this.props;
    const { data, errors, loading, success, warning, SSOWarning } = this.state;
    let { fromPath } = this.props.location.state || { fromPath: { pathname: '/dashboard' } }

    if (success || isAuthenticated) {
      return (
        <Redirect to={fromPath} />
      )
    }
    let noUserFoundFromTPTAuth = false;
    let tptAuthTokenExpired = false;
    if (this.props.location.search) {
      const obj = queryString.parse(this.props.location.search);
      const { loginFailed } = obj;
      if (loginFailed === 'NoUserFound') {
        noUserFoundFromTPTAuth = true;
      }
      else if (loginFailed === 'TokenExpired') {
        tptAuthTokenExpired = true;
      }
    }

    return (
      <div>
        <TopBar />
        <HomeHeader />
        <div id="contentWrapper">
          <div id="home" className="rev_slider_wrapper fullscreen-container">
            <div id="slider" className="rev_slider fullscreenbanner" style={sliderStyle} data-version="5.0.7">
              <ul>
                {!isAuthenticated && (<li data-transition="slideoververtical" data-fstransition="fade">
                  <img src="custom/hd_back.png" alt="" data-bgposition="center center" data-bgfit="cover"
                    data-bgrepeat="no-repeat" data-bgparallax="10" className="rev-slidebg" data-no-retina></img>
                  <div className="tp-caption  main-color tp-resizeme"
                    id="slide-163-layer-6"
                    data-x="['left','left','left','left']" data-hoffset="['880','880','880','800']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['200','-140','-140','-100']"
                    data-lineheight="['70','70','70','40']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                    data-start="50"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    style={tpCaptionSytle2}>
                    <div style={{ "transform": "scale(0.5)" }} >
                      <img alt="" src="custom/client_logo.png" ></img>
                    </div>
                  </div>
                  <div className="tp-caption main-color"
                    id="slide-163-layer-6"
                    data-x="['left','left','left','left']" data-hoffset="['360','360','360','360']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-150','-150','-150','-150']"
                    data-lineheight="['30','30','30','30']"
                    data-width="['500','500','500','500']"
                    data-height="['220','220','220','180']"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                    data-start="50"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="off"
                    style={tpCaptionSytle2}>
                    {noUserFoundFromTPTAuth && (
                      <Message warning>
                        <Message.Header>Login Failed</Message.Header>
                        <p>No user found with associated TPT Auth email.</p>
                      </Message>
                    )}
                    {tptAuthTokenExpired && (
                      <Message warning>
                        <Message.Header>Login Failed</Message.Header>
                        <p>TPT Auth Token expired, please login again.</p>
                      </Message>
                    )}
                    {(warning && warning.global) &&
                      <Message warning>
                        <Message.Header>{warning.global}</Message.Header>
                        <p>Please check your email and click the confirmation link there</p>
                      </Message>}
                    {(errors && errors.global) && <Message negative>
                      <Message.Header>{errors.global}</Message.Header>
                      <p>Please check your credential and login again.</p>
                    </Message>}
                    {(SSOWarning && SSOWarning.global) && <Message warning>
                      <Message.Header>{SSOWarning.global}</Message.Header>
                      <p>Please login with TPT Auth.</p>
                    </Message>}
                    <Dimmer inverted active={loading}>
                      <Loader indeterminate>Logging in, please wait...</Loader>
                    </Dimmer>
                    <Form>
                      <Form.Field required autoComplete="on">
                        <label htmlFor="email">Email or username:</label>
                        <Form.Input
                          error={!!errors && !!errors.email}
                          type="text"
                          name="email"
                          placeholder="example@example.com"
                          value={data.email}
                          onChange={this.onChange}
                        />
                      </Form.Field>
                      {(errors && errors.email) && (<div><InlineError text={errors.email} /><br /></div>)}
                      <Form.Field required autoComplete="on">
                        <label htmlFor="password">Password:</label>
                        <Form.Input
                          error={!!errors && !!errors.password}
                          type="password"
                          id="password"
                          name="password"
                          placeholder="Make it secure"
                          value={data.password}
                          onChange={this.onChange}
                        />
                      </Form.Field>
                      {(errors && errors.password) && (<div><InlineError text={errors.password} /><br /></div>)}
                      <Button primary floated="right" onClick={this.onSubmit}>Login</Button>
                      <Link to="/forgetpassword"
                        style={{ "display": "inline-block", "marginLeft": "10px" }}>
                        Forgot Password?
										</Link>
                    </Form>
                    <br />
                    <Button primary floated="left" onClick={this.handleLoginWithTPTAuth.bind(this)}>
                      Login with TPT Auth
									</Button>
                    <br /><br />
                  </div>
                </li>)}
              </ul>
            </div>
          </div>
          <Footer />
        </div>
        <VersionModel />
        <DisclaimModel />
        <UseModel />
      </div>
    )
  }
}

HomePage.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  login: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.user.token
})

export default connect(mapStateToProps, { login })(HomePage);
