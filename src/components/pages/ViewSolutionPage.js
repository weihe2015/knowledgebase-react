import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';
import axios from 'axios';

import { Message } from '../../utils/reactSemanticUI';
import { connect } from 'react-redux';
import queryString from 'query-string';
import ViewSolutionForm from '../forms/ViewSolutionForm';
import { getSolutionByIdAndTitle } from '../../actions/solutions';
import { validateToken, logout } from '../../actions/auth';
import { TopBar, DisclaimModel, UseModel, VersionModel } from './HomePage';
import Footer from '../navigation/Footer';
import HomeHeader from '../navigation/HomeHeader';
import hdBackgroundImage from '../../img/hd_back.png';
class ViewSolutionPage extends React.Component {
	state = {
		solutionId: null,
		solution: null,
		errors: {}
	}

	componentWillMount = () => {
		const token = localStorage.knowledgebaseJWT;
		this.props.validateToken(token).then(res => {
			const solutionId = queryString.parse(this.props.location.search).solutionId;
			const title = queryString.parse(this.props.location.search).title;

			this.props.getSolutionByIdAndTitle(solutionId, title)
				.then(solution => {
					if (solution) {
						this.setState({
							solutionId,
							solution: solution.data.entities.solutions[solution.data.result]
						})
					}
					else {
						this.setState({ ...this.state, errors: { global: `Cannot fetch Solution` } });
					}
				})
				.catch(err => {
					this.setState({ errors: {global: err.response.data.errors.global} });
				})
		})
		.catch(err => this.props.logout());
	}

	componentWillUnmount = () => {
		this.setState({solutionId: null, solution: null});
	}

  handleLogout = () => {
    // user login with TPT Auth:
    if (localStorage.knowledgebaseTPTOAuthJWT) {
      const id_token = localStorage.knowledgebaseTPTOAuthJWT;
      localStorage.removeItem('knowledgebaseTPTOAuthJWT');
      localStorage.removeItem('knowledgebaseJWT');
      delete axios.defaults.headers.common.authorization;

      const host = window.location.protocol + "//" + window.location.hostname;
      const endSessionURL = `https://sso.transperfect.com/connect/endsession?id_token_hint=${id_token}&post_logout_redirect_uri=${host}`;
      window.location = endSessionURL;
    }
    else {
      this.props.logout();
      window.location.href="/";
    }
  }
  
	render() {
		const { solution, errors } = this.state;
		if (errors.global) {
			return (<Redirect to='/dashboard' />)
		}
		return (
			<div style={{"backgroundImage": `url(${hdBackgroundImage})`}}>
				<TopBar />
				<HomeHeader handleLogout={ this.handleLogout }/>
				{ errors.global && <Message negative>
						<Message.Header>Something went wrong</Message.Header>
						<p>{errors.global}</p>
					</Message>}
				{!!solution && (<ViewSolutionForm solution={solution} />)}
				<VersionModel/>
				<DisclaimModel />
				<UseModel />
				<Footer />
			</div>
		)
	}
}

ViewSolutionPage.propTypes = {
	getSolutionByIdAndTitle: PropTypes.func.isRequired,
	validateToken: PropTypes.func.isRequired,
	logout: PropTypes.func.isRequired
}

export default connect(null, { getSolutionByIdAndTitle,
	validateToken,
	logout })(ViewSolutionPage);
