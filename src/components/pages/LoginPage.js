import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { Segment } from '../../utils/reactSemanticUI';

import LoginForm from '../forms/LoginForm';
import { login } from '../../actions/auth';
import DashboardFooter from '../navigation/DashboardFooter';

class LoginPage extends React.Component {

  state = {
    success: false
  }

  submit = (data) => this.props.login(data).then(() => this.setState({ success: true }));

  render() {
    const { success } = this.state;
    let { fromPath } = this.props.location.state || { fromPath: { pathname: '/dashboard' } }

    return (
      <div>
        {success ? <Redirect to={fromPath} /> : (
          <div className="ui container" style={{ "marginTop": "50px" }}>
            <h1 style={{ "textAlign": "center" }}>Login</h1>
            <Segment>
              <LoginForm submit={this.submit} />
            </Segment>
            <DashboardFooter />
          </div>
        )}
      </div>
    )
  }
}

LoginPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  login: PropTypes.func.isRequired
};

export default connect(null, { login })(LoginPage);
