import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Message, Segment } from '../../utils/reactSemanticUI';
import ForgotPasswordForm from '../forms/ForgotPasswordForm';
import { resetPasswordRequest } from '../../actions/auth';
import { TopBar, DisclaimModel, UseModel } from './HomePage';
import HomeHeader from '../navigation/HomeHeader';
import hdBackgroundImage from '../../img/hd_back.png';
import Footer from '../navigation/Footer';

class ForgetPasswordPage extends React.Component {
	state= {
		success: false
	}

	submit = data => this.props.resetPasswordRequest(data).then(() => {
		this.setState({success: true});
		setTimeout(() => {
			window.location = window.location.origin;
		}, 3000);
	});
  
	render() {
		return (
      <div style={{"backgroundImage": `url(${hdBackgroundImage})`}}>
      <TopBar/>
      <DisclaimModel/>
		  <UseModel/>
      <HomeHeader />
			<div style={{"margin": "100px auto 100px auto", "width": "800px"}}>
				{ (this.state.success) ? (
						<Message>Reset Password Email has been sent. User will be redirect to main page in 3 seconds.</Message>) :
					(<Segment>
						<ForgotPasswordForm submit={this.submit} />
					</Segment>)
				}
			</div>
      <Footer />
      </div>
		);
	}
}

ForgetPasswordPage.propTypes = {
	resetPasswordRequest: PropTypes.func.isRequired
}

export default connect(null, { resetPasswordRequest })(ForgetPasswordPage);