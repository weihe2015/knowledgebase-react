import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';
import axios from 'axios';
import { Button, Comment, Dimmer, Form, Grid,
	Loader, Message, Modal } from '../../utils/reactSemanticUI';
import { allSolutionsSelector } from '../../reducers/solutions';
import {
	createSolution,
	fetchSolutions, fetchNewSolutionId,
	insertImageToContent,
	updateSolutionByAttachment
} from '../../actions/solutions';
import { validateToken, logout } from '../../actions/auth';

import SolutionMainPage from './SolutionMainPage';
import moment from 'moment';
import Select from 'react-select';
import InlineError from '../messages/InlineError';
import Footer from '../navigation/Footer';
import { dateOptions, visibilityOptions } from '../common';
import UploadFileForm from '../forms/UploadFileForm';
import DatePicker from 'react-datepicker';

// import React Draft Wysiwyg
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import '../../css/solutionForm.css';
import 'react-select/dist/react-select.css';
import DashboardHeader from '../navigation/DashboardHeader';
import { TopBar, DisclaimModel, UseModel, VersionModel } from './HomePage';

class DashboardPage extends React.Component {

	state = {
		solutions: null,
		topic: "All Solutions",
		modalIsOpen: false,
		errors: {},
		data: {
			_id: null,
			solutionId: -1,
			title: "",
			topic: "",
			content: "",
			visibility: "private",
			status: "Draft",
			keywords: "",
			expirationDate: moment(),
			viewNumber: 0,
			ratingUp: 0,
			ratingDown: 0,
			files: [],
			history: []
		},
		dateOption: "never",
		editorState: EditorState.createEmpty(),
		editorUploadedFiles: [],
		success: false
	}

	onChange = e => {
		this.setState({
			...this.state,
			errors: {},
			data: { ...this.state.data, [e.target.name]: e.target.value }
		})
	}

	handleDateChange = date => {
		this.setState({
			...this.state,
			data: { ...this.state.data, expirationDate: date }
		});
	}

	handleDateOptionChange = option => {
		if (option === 'specific') {
			if (!this.state.data.expiration) {
				this.setState({
					...this.state,
					data: { ...this.state.data, expiration: moment() },
					dateOption: option.value
				});
			} else {
				this.setState({
					...this.state,
					data: { ...this.state.data },
					dateOption: option.value
				});
			}
		}
		else {
			this.setState({
				...this.state,
				data: { ...this.state.data, expirationDate: null },
				dateOption: option.value
			});
		}
	}

	handleTopicChange = selectedOption => {
		this.setState({
			...this.state,
			data: { ...this.state.data, topic: selectedOption.value }
		})
	}

	handleVisibilityChange = selectedOption => {
		this.setState({
			...this.state,
			data: { ...this.state.data, visibility: selectedOption.value }
		})
	}

	componentWillMount = () => {
		const token = localStorage.knowledgebaseJWT;
		const { isClient } = this.props;
		this.props.validateToken(token).then(res => {
			if (this.props.location.search) {
				const topic = queryString.parse(this.props.location.search).topic;
				// const viewName = queryString.parse(this.props.location.search).viewName;
				// User search by topic:
				if (topic) {
					if (isClient) {
						const userType = 'client';
						this.props.fetchSolutions(userType).then(solutions => {
							if (solutions) {
								this.setSolutionState(solutions, topic);
							}
						});
					}
					else {
						this.props.fetchNewSolutionId().then(solutionIdData => {
							this.setState({
								...this.state,
								data: { ...this.state.data, solutionId: solutionIdData.data }
							})
						})
						.catch(err => {
								this.setState({ ...this.state, errors: { global: `Cannot fetch new Solution ID` } });
						});
						const userType = 'admin';
						this.props.fetchSolutions(userType).then(solutions => {
							if (solutions) {
								this.setSolutionState(solutions, topic);
							}
						});
					}
				}
			}
			else {
				if (isClient) {
					const userType = 'client';
					this.props.fetchSolutions(userType).then(solutions => {
						if (solutions) {
							this.setSolutionState(solutions);
						}
					});
				}
				else {
					this.props.fetchNewSolutionId().then(solutionIdData => {
						this.setState({
							...this.state,
							data: { ...this.state.data, solutionId: solutionIdData.data }
						})
					})
					.catch(err => {
							this.setState({ ...this.state, errors: { global: `Cannot fetch new Solution ID` } });
					});
					const userType = 'admin';
					this.props.fetchSolutions(userType).then(solutions => {
						if (solutions) {
							this.setSolutionState(solutions);
						}
					});
				}
			}
		})
		.catch(errors => {
			this.setState({ ...this.state, errors });
		});
	}

	onEditorStateChange = (editorState) => {
		const htmlContent = draftToHtml(convertToRaw(editorState.getCurrentContent()));
		this.setState({
			...this.state,
			data: { ...this.state.data, content: htmlContent },
			editorState
		});
	}

	setSolutionState = (solutions, topic) => {
		let solutionArray = [];
		const { isClient, isInternal, isAdmin, isGLTech, products } = this.props;
		if (isClient) {
			solutions.data.result.forEach((solutionId) => {
				const solution = solutions.data.entities.solutions[solutionId];
				if (products.includes(solution.topic) && solution.visibility === 'public') {
					solutionArray.push(solution);
				}
			})
		}
		else if (isInternal) {
			solutions.data.result.forEach((solutionId) => {
				const solution = solutions.data.entities.solutions[solutionId];
				if (solution.visibility === 'private') {
					solutionArray.push(solution);
				}
			})
		}
		// GLKB-100: filter out tech_senior solutions for admin and gl_tech users
		else if (isAdmin || isGLTech) {
			solutions.data.result.forEach((solutionId) => {
				const solution = solutions.data.entities.solutions[solutionId];
				if (solution.visibility !== 'tech_senior') {
					solutionArray.push(solution);
				}
			})
		}
		else {
			solutions.data.result.forEach((solutionId) => {
				solutionArray.push(solutions.data.entities.solutions[solutionId]);
			})
		}
		this.setState({
			solutions: solutionArray,
			topic
		})
	}

	// handle image upload on <Editor> component
	uploadImageCallBack = (file) => {
		return new Promise((resolve, reject) => {
			let { editorUploadedFiles } = this.state;
			editorUploadedFiles.push(file);
			let url = "https://glsupport-kb.translations.com";

			// upload file to server and return an url.
			this.props.insertImageToContent(file)
				.then(res => {
					url += res.data;
					this.setState({ editorUploadedFiles, url });
					resolve({ data: { link: url } });
				})
				.catch(err => {
					console.log(err);
					reject();
				});
		});
	}

	toggleModal = () => {
		this.setState({ ...this.state, success: false, modalIsOpen: !this.state.modalIsOpen });
	}

	onSubmit = e => {
		e.preventDefault();
		const errors = this.validate(this.state.data);
		this.setState({ ...this.state, errors, modalIsOpen: true, success: false });
		if (Object.keys(errors).length === 0) {
			this.setState({ loading: true });
			const solution = this.state.data;
			const { topic } = this.state;
			// handle upload file in add solution action:
			if (solution.hasOwnProperty('tmpFiles')) {
				const files = solution.tmpFiles;
				delete solution.tmpFiles;
				this.props.createSolution(solution)
					.then(() => {
						// update images to solution:
						let promises = [];
						files.forEach(async (file) => {
							const promise = this.props.updateSolutionByAttachment(solution, file);
							promises.push(promise);
						});
						Promise.all(promises)
							.then(res => {
								this.props.fetchNewSolutionId()
									.then(res2 => {
										this.setState({
											...this.state, modalIsOpen: false, success: true,
											data: {
												_id: null,
												solutionId: res2.data,
												title: "",
												topic: "",
												content: "",
												visibility: "private",
												status: "Draft",
												keywords: "",
												expirationDate: moment(),
												viewNumber: 0,
												ratingUp: 0,
												ratingDown: 0,
												files: [],
												history: []
											}, dateOption: "never", editorUploadedFiles: [], editorState: EditorState.createEmpty()
										});
									});
								// only admin and internal user have ability to create solutions.
								// No need to fetch solutions only for client.
								// refetch all solutions:
								const userType = 'admin';
								this.props.fetchSolutions(userType).then(solutions => {
									if (solutions) {
										this.setSolutionState(solutions, topic);
									}
								});
							})
							.catch(err => this.setState({
								modalIsOpen: true, success: false,
								errors: { global: err.message }
							}));
					})
					.catch(err => {
						this.setState({
							modalIsOpen: true, success: false,
							errors: { global: err.message }
						});
					});
			}
			else {
				this.props.createSolution(solution)
					.then((res) => {
						this.props.fetchNewSolutionId()
							.then(res2 => {
								this.setState({
									...this.state, modalIsOpen: false, success: true,
									data: {
										_id: null,
										solutionId: res2.data,
										title: "",
										topic: "",
										content: "",
										visibility: "private",
										status: "Draft",
										keywords: "",
										expirationDate: moment(),
										viewNumber: 0,
										ratingUp: 0,
										ratingDown: 0,
										files: [],
										history: []
									}, dateOption: "never", editorUploadedFiles: [], editorState: EditorState.createEmpty()
								});
							});
						// only admin and internal user have ability to create solutions.
						// No need to fetch solutions only for client.
						// refetch all solutions:
						const userType = 'admin';
						this.props.fetchSolutions(userType).then(solutions => {
							if (solutions) {
								this.setSolutionState(solutions, topic);
							}
						});
					})
					.catch(err => { this.setState({ ...this.state, success: false, errors: { global: err.message } }) });
			}
		}
	}

	validate = data => {
		const errors = {};
		if (!data.title) {
			errors.title = "Solution Title cannot be blank";
		}
		// Solution topic:
		if (!data.topic) {
			errors.topic = "Solution Topic cannot be blank";
		}
		// Solution content:
		if (!data.content || data.content === "<p></p>") {
			errors.pages = "Solution content cannot be blank";
		}
		return errors;
	}

  handleLogout = () => {
    // user login with TPT Auth:
    if (localStorage.knowledgebaseTPTOAuthJWT) {
      const id_token = localStorage.knowledgebaseTPTOAuthJWT;
      localStorage.removeItem('knowledgebaseTPTOAuthJWT');
      localStorage.removeItem('knowledgebaseJWT');
      delete axios.defaults.headers.common.authorization;

      const host = window.location.protocol + "//" + window.location.hostname;
      const endSessionURL = `https://sso.transperfect.com/connect/endsession?id_token_hint=${id_token}&post_logout_redirect_uri=${host}`;
      window.location = endSessionURL;
    }
    else {
      this.props.logout();
      window.location.href="/";
    }
  }

	render() {
		const { solutions, topic, errors, success,
			data, dateOption, editorState, modalIsOpen } = this.state;
		const { isConfirmed } = this.props;
		let { expirationDate } = this.state;

		if (moment(data.expirationDate).isValid()) {
			expirationDate = moment(data.expirationDate).format("YYYY-MM-DD hh:mm A");
		}
		else {
			expirationDate = "-";
		}

		return (
			<div>
				<TopBar />
				<DashboardHeader toggleModal={this.toggleModal} handleLogout={ this.handleLogout }/>
				<Modal open={modalIsOpen} onClose={this.toggleModal} size="fullscreen" dimmer="blurring"
					style={{
						overflowY: "scroll"
					}}>
					<Modal.Header>Add New Solution</Modal.Header>
					<Modal.Content>
						<div style={{"height":"810px"}}>
						<Form >
							{(!success && errors && errors.global) && (
								<Message negative>
									<Message.Header>Something went wrong</Message.Header>
									<p>{errors.global}</p>
								</Message>
							)}
							<Grid columns={2}>
								<Grid.Row>
									<Grid.Column>
										<div style={{ "marginLeft": "30px" }} >
											<Form.Field error={!!errors.title} required>
												<label htmlFor="title">Solution Title</label>
												<input
													type="text"
													id="title"
													name="title"
													placeholder="Title"
													value={data.title ? data.title : ""}
													onChange={this.onChange}
												/>
												{errors.title && <InlineError text={errors.title} />}
											</Form.Field>

											<Grid columns={2}>
												<Grid.Row>
													<Grid.Column>
														<div style={{"marginLeft": "15px"}} >
															<Form.Field error={!!errors.topic} required>
																<label htmlFor="topic">Topic</label>
																<Select
																	name="topic"
																	searchable={false}
																	value={data.topic ? data.topic : ""}
																	onChange={this.handleTopicChange}
																	options={this.props.topicOptions}
																/>
																{errors.topic && <InlineError text={errors.topic} />}
															</Form.Field>
														</div>
													</Grid.Column>
													<Grid.Column>
														<Form.Field required>
															<label htmlFor="visibility">Visibility</label>
															<Select
																name="visibility"
																searchable={false}
																clearable={false}
																value={data.visibility ? data.visibility : "private"}
																onChange={this.handleVisibilityChange}
																options={visibilityOptions}
															/>
														</Form.Field>
													</Grid.Column>
												</Grid.Row>
											</Grid>
											<br/>
											<Form.Field >
												<label htmlFor="keywords">Keywords</label>
												<input
													type="text"
													id="keywords"
													name="keywords"
													placeholder="Keywords"
													value={data.keywords ? data.keywords : ""}
													onChange={this.onChange}
												/>
												<Comment>
													<Comment.Content>
														<Comment.Text>
															<p style={{ "color": "#a9a9a9" }}>* Keywords should be comma separated.</p>
														</Comment.Text>
														<Comment.Text>
															<p style={{ "color": "#a9a9a9" }}>
																* * Choosing a relevent keyword for a solution will improve its search capability:
																For example, Printer, toner, paper</p>
														</Comment.Text>
													</Comment.Content>
												</Comment>
											</Form.Field>
											<UploadFileForm solution={data} />
											<Form.Field>
												<label htmlFor="expiration">Expire On</label>
												<Grid celled>
													<Grid.Row centered>
														<Grid.Column width={3}>
															<Select
																name="dateTopic"
																searchable={false}
																clearable={false}
																value={dateOption}
																onChange={this.handleDateOptionChange.bind(this)}
																options={dateOptions}
															/>
														</Grid.Column>
														{(dateOption === 'specific') && (
															<Grid.Column width={6}>
																<DatePicker
																	name="date"
																	showTimeSelect
																	timeFormat="HH:mm"
																	timeIntervals={30}
																	dateFormat="YYYY-MM-DD h:mm A"
																	minDate={moment()}
																	selected={!moment(expirationDate).isValid() ?
																		moment() : moment(new Date(expirationDate))}
																	onChange={this.handleDateChange.bind(this)}
																/>
															</Grid.Column>)}
													</Grid.Row>
												</Grid>
											</Form.Field>
										</div>
									</Grid.Column>
									<Grid.Column>
										<Form.Field error={!!errors.content} >
											<label htmlFor="topic">Content</label>
											<Editor
												editorState={editorState}
												wrapperClassName="demo-wrapper"
												editorClassName="demo-editor"
												onEditorStateChange={this.onEditorStateChange}
												toolbar={{
													image: {
														previewImage: true,
														uploadCallback: this.uploadImageCallBack,
														alt: { present: true, mandatory: false }
													}
												}}
											/>
											{errors.content && <InlineError text={errors.content} />}
										</Form.Field>
									</Grid.Column>
								</Grid.Row>
							</Grid>
						</Form>
						</div>
					</Modal.Content>
					<Modal.Actions>
						<Button primary onClick={this.onSubmit}>Save</Button>
						<Button type="link-cancel" onClick={this.toggleModal}>Close</Button>
					</Modal.Actions>
				</Modal>
				<VersionModel/>
				<DisclaimModel />
				<UseModel />
				<section>
					{success && (
						<div style={{ "textAlign": "center" }}>
							<Message positive>
								<Message.Header>Solution Creation</Message.Header>
								<p>Solution is created successfully!.</p>
							</Message>
						</div>
					)}
					{(isConfirmed && !solutions) ? (
						<Dimmer inverted active={true}>
							<Loader indeterminate>Loading solutions</Loader>
						</Dimmer>) :
						(isConfirmed &&
							<SolutionMainPage solutions={solutions} topic={topic} />)}
				</section>
				<br /><br /><br /><br />
				<Footer />
			</div>
		)
	}
}


DashboardPage.propTypes = {
	createSolution: PropTypes.func.isRequired,
	fetchNewSolutionId: PropTypes.func.isRequired,
	updateSolutionByAttachment: PropTypes.func.isRequired,
	fetchSolutions: PropTypes.func.isRequired,
	solutions: PropTypes.arrayOf(PropTypes.shape({
		title: PropTypes.string.isRequired
	}).isRequired).isRequired,
	validateToken: PropTypes.func.isRequired,
	logout: PropTypes.func.isRequired,
	isAuthenticated: PropTypes.bool.isRequired,
	isConfirmed: PropTypes.bool.isRequired,
	isClient: PropTypes.bool.isRequired,
	isAdmin: PropTypes.bool.isRequired,
	products: PropTypes.arrayOf(PropTypes.string)
}

const mapStateToProps = (state) => ({
	isConfirmed: !!state.user.confirmed,
	isAuthenticated: !!state.user.email,
	isClient: state.user.role === 'client',
	isInternal: state.user.role === 'internal',
	isAdmin: state.user.role === 'admin',
	isGLTech: state.user.role === 'gl_tech',
	products: state.user.role === 'client' ? state.user.products : [],
	solutions: allSolutionsSelector(state),
	topicOptions: state.products.topicOptions
})

export default connect(mapStateToProps, {
	createSolution,
	fetchSolutions,
	fetchNewSolutionId,
	insertImageToContent,
	updateSolutionByAttachment,
	validateToken,
	logout
})(DashboardPage);


