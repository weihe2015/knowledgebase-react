import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import axios from 'axios';
import { Message, Dimmer, Loader } from '../../utils/reactSemanticUI';
import ChangePasswordForm from '../forms/ChangePasswordForm';
import { validateToken, changePassword, logout } from '../../actions/auth';
import Footer from '../navigation/Footer';
import HomeHeader from '../navigation/HomeHeader';
import { TopBar,DisclaimModel,UseModel } from './HomePage';
import hdBackgroundImage from '../../img/hd_back.png';
class ChangePasswordPage extends React.Component {

  state = {
    loading: true,
    success: false
  }

  componentDidMount() {
    this.props.validateToken(this.props.token)
      .then(() => this.setState({ loading: false, success: true }))
      .catch(() => this.setState({ loading: false, success: false }));
  }

  handleLogout = () => {
    // user login with TPT Auth:
    if (localStorage.knowledgebaseTPTOAuthJWT) {
      const id_token = localStorage.knowledgebaseTPTOAuthJWT;
      localStorage.removeItem('knowledgebaseTPTOAuthJWT');
      localStorage.removeItem('knowledgebaseJWT');
      delete axios.defaults.headers.common.authorization;

      const host = window.location.protocol + "//" + window.location.hostname;
      const endSessionURL = `https://sso.transperfect.com/connect/endsession?id_token_hint=${id_token}&post_logout_redirect_uri=${host}`;
      window.location = endSessionURL;
    }
    else {
      this.props.logout();
      window.location.href="/";
    }
  }
  

  submit = data => this.props.changePassword(data).then(() => this.props.history.push('/dashboard'));

  render() {
    const { loading, success } = this.state;
    const { token } = this.props;
    return (
      <div style={{"backgroundImage": `url(${hdBackgroundImage})`}}>
        <TopBar/>
				<HomeHeader handleLogout={ this.handleLogout }/>
        <DisclaimModel/>
				<UseModel/>
        <div style={{"margin": "50px auto 100px auto", "width": "800px"}}>
          <h3 style={{"textAlign": "center"}}>Change Password</h3>
          <Dimmer inverted active={loading}>
            <Loader indeterminate>Loading Form</Loader>
          </Dimmer>
          { !loading && success && <ChangePasswordForm submit={this.submit} token={token}/>}
          { !loading && !success && <Message>Invalid Token</Message>}
        </div>
        <Footer />
      </div>
    )
  }
}

ChangePasswordPage.propTypes = {
	validateToken: PropTypes.func.isRequired,
  changePassword: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	}).isRequired
}

const mapStateToProps = (state) => ({
	token: state.user.token
})

export default connect(mapStateToProps, { validateToken, changePassword, logout })(ChangePasswordPage);
