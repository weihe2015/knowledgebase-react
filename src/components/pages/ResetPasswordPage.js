import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Message, Dimmer, Loader } from '../../utils/reactSemanticUI';
import ResetPasswordForm from '../forms/ResetPasswordForm';
import { validateToken, resetPassword } from '../../actions/auth';
import DashboardFooter from '../navigation/DashboardFooter';

class ResetPasswordPage extends React.Component {
	state = {
		loading: true,
		success: false
	}

	componentDidMount() {
		this.props.validateToken(this.props.match.params.token)
			.then(() => this.setState({ loading: false, success: true }))
			.catch(() => this.setState({ loading: false, success: false }));
	}

	submit = data => this.props.resetPassword(data).then((res) => this.props.history.push('/dashboard'));

	render() {
		const { loading, success } = this.state;
		const token = this.props.match.params.token;
		return (
			<div style={{"margin": "100px auto 100px auto", "width": "800px"}}>
				<h3 style={{"textAlign": "center"}}>Reset Password</h3>
				<Dimmer inverted active={loading}>
					<Loader indeterminate>Loading Form</Loader>
				</Dimmer>
				{ !loading && success && <ResetPasswordForm submit={this.submit.bind(this)} token={token} />}
				{ !loading && !success && <Message>Invalid Token</Message>}
				<DashboardFooter />
			</div>
		)
	}
}

ResetPasswordPage.propTypes = {
	validateToken: PropTypes.func.isRequired,
	match: PropTypes.shape({
		params: PropTypes.shape({
			token: PropTypes.string.isRequired
		}).isRequired
	}).isRequired,
	resetPassword: PropTypes.func.isRequired,
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	}).isRequired
}

export default connect(null, { validateToken, resetPassword })(ResetPasswordPage);
