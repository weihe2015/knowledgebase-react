import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import axios from 'axios';

import { Message } from '../../utils/reactSemanticUI';
import ModifyUserForm from '../forms/ModifyUserForm';

import Footer from '../navigation/Footer';
import HomeHeader from '../navigation/HomeHeader';
import { TopBar,DisclaimModel,UseModel } from './HomePage';
import hdBackgroundImage from '../../img/hd_back.png';
import { logout } from '../../actions/auth';

class ModifyUserPage extends React.Component {

  handleLogout = () => {
    // user login with TPT Auth:
    if (localStorage.knowledgebaseTPTOAuthJWT) {
      const id_token = localStorage.knowledgebaseTPTOAuthJWT;
      localStorage.removeItem('knowledgebaseTPTOAuthJWT');
      localStorage.removeItem('knowledgebaseJWT');
      delete axios.defaults.headers.common.authorization;

      const host = window.location.protocol + "//" + window.location.hostname;
      const endSessionURL = `https://sso.transperfect.com/connect/endsession?id_token_hint=${id_token}&post_logout_redirect_uri=${host}`;
      window.location = endSessionURL;
    }
    else {
      this.props.logout();
      window.location.href="/";
    }
  }
  
  render() {
    const { isConfirmed } = this.props;
    return (
      <div style={{"backgroundImage": `url(${hdBackgroundImage})`}}>
        <TopBar/>
				<HomeHeader handleLogout={ this.handleLogout }/>
        <DisclaimModel/>
				<UseModel/>
        {(isConfirmed) ? (
          <div>
            <div style={{"marginTop": "20px", "marginLeft": "20px", "marginBottom": "50px", "marginRight": "20px"}}>
              <ModifyUserForm />
            </div>
          </div>
        ) : (
        <div>
          <Message negative></Message>
            <Message.Header>Something went wrong</Message.Header>
            <p>Please confirm your email first.</p>
        </div>
        )}
        <Footer />
    </div>
    )
  }
}

ModifyUserPage.propTypes = {
  isConfirmed: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  isConfirmed: !!state.user.confirmed
})

export default connect(mapStateToProps, { logout })(ModifyUserPage);
