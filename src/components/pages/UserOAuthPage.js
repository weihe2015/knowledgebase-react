import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import queryString from 'query-string';
import decode from 'jwt-decode';
import { loginWithOAuth2 } from '../../actions/auth';

class UserOAuthPage extends React.Component {
  constructor(props) {
    super(props);
    
    const responseObj = queryString.parse(props.location.hash);
    if (Object.keys(responseObj).length > 0) {
      const { id_token } = responseObj;
      const payload = decode(id_token);
      const currentDate = new Date().getTime() / 1000;
      const expiredDate = payload.exp;
      // check if this jwt is expired. If it is, redirect user to login page.
      if (expiredDate < currentDate) {
        props.history.push('/?loginFailed=TokenExpired');
      }
      else {
        // Token is not expired, check with server:
        let credentials = {};
        credentials.email = payload.sub;
        props.loginWithOAuth2(credentials).then(() => {
          localStorage.setItem('knowledgebaseTPTOAuthJWT', id_token);
          props.history.push('/dashboard');
        })
        // User email not found.
        .catch(err => {
          props.history.push('/?loginFailed=NoUserFound');
        })
      }
    }
  }

  render() {
    let { fromPath } = this.props.location.state || { fromPath: { pathname: '/dashboard' } }

    return (
      <Redirect to={fromPath} />
    )
  }
}

UserOAuthPage.propTypes = {
	loginWithOAuth2: PropTypes.func.isRequired
}

export default connect(null, { loginWithOAuth2 })(withRouter(UserOAuthPage));