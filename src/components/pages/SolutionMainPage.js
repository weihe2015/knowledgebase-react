import React from 'react';
import ReactNbsp from 'react-nbsp';
import _ from 'lodash';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button, Checkbox, Dimmer, Icon, Input, Grid, Label,
  Loader, Menu, Message, Popup, Segment, Sidebar
} from '../../utils/reactSemanticUI';
import { base64ToArrayBuffer, saveByteArray } from '../../utils/downloadFile';
import {
  fetchSolutions,
  searchSolutions,
  getSolutionByIdAndTitle,
  downloadAllAttachmentsBySolutionId,
  deleteSolution
} from '../../actions/solutions';
import { fetchProducts } from '../../actions/products';
import { includeElement, filterMethod } from '../common';
// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";
import ViewSimpleSolution from '../forms/ViewSimpleSolution';

class SolutionMainPage extends React.Component {
  state = {
    selectedTopic: "All Solutions",
    selectedSolution: null,
    selectedRows: [],
    solutions: this.props.solutions,
    visible: true,
    errors: {},
    tableWidth: 12,
    loading: false,
    searchLoading: false,
    typingTimeout: 0,
    success: false,
    deleteSuccss: false,
    showMoreFilters: false,
    searchSolutionOnBothTitleAndContent: false,
    query: ''
  }

  componentWillReceiveProps = props => {
    this.setState({
      solutions: props.solutions
    })
  }

  componentWillMount = () => {
    this.props.fetchProducts();
    const { topic } = this.props;
    if (topic && topic !== 'All Solutions') {
      this.setState({ ...this.state, selectedTopic: topic });
    }
  }

  selectSolutionByTopic = (e) => {
    if (!e.target) {
      return;
    }
    const ahrefElement = e.target.querySelectorAll('a')[0];
    const url = ahrefElement.getAttribute('href');
    this.props.history.push(url);
  }

  componentWillUnmount = () => {
    this.setState({ solution: null });
  }

  refetchSolutionsList = e => {
    e.preventDefault();
    e.stopPropagation();
    const { isClient } = this.props;
    this.setState({ ...this.state, loading: true, success: false, deleteSuccss: false });
    if (isClient) {
      const userType = 'client';
      this.props.fetchSolutions(userType).then(solutions => {
        if (solutions) {
          this.setSolutionState(solutions, true);
        }
      });
    }
    else {
      const userType = 'admin';
      this.props.fetchSolutions(userType).then(solutions => {
        if (solutions) {
          this.setSolutionState(solutions, true);
        }
      });
    }
  }

  unSelectSolution = e => {
    e.preventDefault();
    this.setState({
      ...this.state, success: false, selectedRows: [], deleteSuccss: false,
      tableWidth: 12, selectedSolution: null, visible: true
    });
  }

  setSolutionState = (solutions, isRefetchSolutions) => {
    let solutionArray = [];
    const { isClient, isInternal, isGLTech, isAdmin, products } = this.props;
    if (isClient) {
      solutions.data.result.forEach((solutionId) => {
        const solution = solutions.data.entities.solutions[solutionId];
        if (products.includes(solution.topic) && solution.visibility === 'public') {
          solutionArray.push(solution);
        }
      })
    }
    else if (isInternal) {
      solutions.data.result.forEach((solutionId) => {
        const solution = solutions.data.entities.solutions[solutionId];
        if (solution.visibility === 'private') {
          solutionArray.push(solution);
        }
      })
    }
    // If user role is admin or gl_tech, we exclude solution which has visibility of "tech_senior":
    // GLKB-100: filter out tech_senior solutions for admin and gl_tech users
    else if (isAdmin || isGLTech) {
      solutions.data.result.forEach((solutionId) => {
        const solution = solutions.data.entities.solutions[solutionId];
        if (solution.visibility !== 'tech_senior') {
          solutionArray.push(solution);
        }
      })
    }
    else {
      solutions.data.result.forEach((solutionId) => {
        solutionArray.push(solutions.data.entities.solutions[solutionId]);
      })
    }
    this.setState({
      ...this.state,
      solutions: solutionArray,
      loading: false,
      success: isRefetchSolutions
    })
  }

  handleDeleteSolution = async (solution) => {
    this.setState({
      ...this.state, success: false, selectedRows: [], loading: true, deleteSuccss: false,
      tableWidth: 12, selectedSolution: null, visible: true
    });

    await this.props.deleteSolution(solution);
    const userType = 'admin';
    this.props.fetchSolutions(userType).then(res => {
      const solutions = res;
      let solutionArray = [];
      if (solutions) {
        solutions.data.result.forEach((solutionId) => {
          solutionArray.push(solutions.data.entities.solutions[solutionId]);
        })
      }
      this.setState({
        ...this.state, success: false, loading: false, deleteSuccss: true, solutions: solutionArray,
        errors: {}, tableWidth: 12, selectedSolution: null, visible: true
      });
    })
      .catch(err => {
        this.setState({
          ...this.state, success: false, loading: false, deleteSuccss: false,
          errors: err.response.data.errors, tableWidth: 12, selectedSolution: null, visible: true
        });
      })
  }

  downloadAttachmentBySolutionId = (solutionId) => {
    this.props.downloadAllAttachmentsBySolutionId(solutionId)
      .then(res => saveByteArray(res.data.filename, base64ToArrayBuffer(res.data.data), res.data.mimetype))
      .catch(err => this.setState({ errors: err }));
  }

  toggleVisibility = () => this.setState({ visible: !this.state.visible })

  toggleShowMoreFilterOptions = () => {
    this.setState({
      ...this.state,
      showMoreFilters: !this.state.showMoreFilters
    });
  }

  toggleSearchSolutionOnBothTitleAndContent = () => {
    const { searchSolutionOnBothTitleAndContent } = this.state;
    // If user wants to search solution on both title and content, refetch all solutions, and clear search query:
    if (!searchSolutionOnBothTitleAndContent) {
      const { isClient } = this.props;
      this.setState({ ...this.state, loading: true, success: false, deleteSuccss: false });
      if (isClient) {
        const userType = 'client';
        this.props.fetchSolutions(userType).then(solutions => {
          if (solutions) {
            this.setSolutionState(solutions, false);
          }
        });
      }
      else {
        const userType = 'admin';
        this.props.fetchSolutions(userType).then(solutions => {
          if (solutions) {
            this.setSolutionState(solutions, false);
          }
        });
      }
      this.setState({
        ...this.state,
        query: '',
        searchSolutionOnBothTitleAndContent: !this.state.searchSolutionOnBothTitleAndContent
      });
    }
    else {
      this.setState({
        ...this.state,
        searchSolutionOnBothTitleAndContent: !this.state.searchSolutionOnBothTitleAndContent
      });
    }
  }

  handleItemClick = e => {
    e.preventDefault();
    if (!e.target) {
      return;
    }
    if (e.target.tagName === 'A') {
      const classAttribute = e.target.getAttribute('class');
      if (classAttribute.indexOf("active item") !== -1) {
        if (classAttribute.indexOf("item") === -1) {
          this.setState({ ...this.state, selectedTopic: classAttribute, selectedRows: [], selectedSolution: null, tableWidth: 12 });
        }
        else {
          const selectedTopic = classAttribute.substring(12);
          this.setState({ ...this.state, selectedTopic, selectedRows: [], selectedSolution: null, });
        }
      }
      else {
        if (classAttribute.indexOf("item") === -1) {
          this.setState({ ...this.state, selectedTopic: classAttribute, selectedRows: [], selectedSolution: null, tableWidth: 12 });
        }
        else {
          const selectedTopic = classAttribute.substring(5);
          this.setState({ ...this.state, selectedTopic, selectedRows: [], selectedSolution: null, tableWidth: 12 });
        }
      }
    }
    else if (e.target.tagName === 'I') {
      const classAttribute = e.target.getAttribute('class');
      if (classAttribute.indexOf("folder outline icon") !== -1) {
        const selectedTopic = classAttribute.substring(20);
        this.setState({ ...this.state, selectedTopic, selectedRows: [], selectedSolution: null, tableWidth: 12 });
      }
      else if (classAttribute.indexOf("folder open icon") !== -1) {
        const selectedTopic = classAttribute.substring(17);
        this.setState({ ...this.state, selectedTopic, selectedRows: [], selectedSolution: null, tableWidth: 12 });
      }
      else {
        this.setState({ ...this.state, selectedTopic: classAttribute, selectedRows: [], selectedSolution: null, tableWidth: 12 });
      }
    }
  }

  handleSearchBarChange = (value) => {
    if (this.typingTimeout) {
      clearTimeout(this.typingTimeout);
    }
    this.setState({ ...this.state, query: value, searchLoading: false });
    this.typingTimeout = setTimeout(() => {
      //search function
      const { query } = this.state;
      this.setState({ ...this.state, searchLoading: true });
      this.props.searchSolutions(query).then(solutions => {
        if (solutions) {
          this.setSolutionState(solutions, false);
        }
        this.setState({ ...this.state, searchLoading: false });
      })
        .catch(err => {
          this.setState({ ...this.state, searchLoading: false });
        });
    }, 500);
  }

  render() {
    const { topic, errors, tableWidth, selectedSolution, loading, searchLoading, success, searchSolutionOnBothTitleAndContent,
      deleteSuccss, visible, selectedTopic, showMoreFilters, query } = this.state;
    const { isClient, products } = this.props;
    let { solutions } = this.state;
    let productOptions = this.props.topicOptions;

    if (isClient) {
      productOptions = _.filter(productOptions, productOption => {
        return products.includes(productOption.text);
      })
    }
    // filter solutions by product selected by user
    if (selectedTopic !== "All Solutions") {
      solutions = _.filter(solutions, solution => {
        return solution.topic === selectedTopic;
      });
    }
    // GLKB-51 Sort solutions by title
    solutions = _.sortBy(solutions, solution => {
      return solution.title.trim();
    });

    //const n = new Date().getTimezoneOffset();
    let { selectedRows } = this.state;
    return (
      <div style={{ "marginRight": "30px", "marginLeft": "30px" }}>
        {errors.global && (<Message negative>
          <Message.Header>Something went wrong</Message.Header>
          <p>{errors.global}</p>
        </Message>)}
        <Dimmer inverted active={loading}>
          <Loader indeterminate>Fetching Solutions, please wait..</Loader>
        </Dimmer>
        <Dimmer inverted active={searchLoading}>
          <Loader indeterminate>Searching Solutions, please wait..</Loader>
        </Dimmer>
        {errors.global && <Message negative>
          <Message.Header>Something went wrong</Message.Header>
          <p>{errors.global}</p>
        </Message>
        }
        <Grid columns={2}>
          <Grid.Row>
            <Grid.Column floated='left' width={2}>
              <div style={{ "margin": "15px 0px 0px 15px" }}>
                <Button onClick={this.toggleVisibility} color="grey">
                  {!visible ? <Icon name="angle right" /> : <Icon name="angle left" />}
                </Button>
              </div>
            </Grid.Column>
            <Grid.Column floated='left' width={4}>
              <div style={{ "margin": "30px 0px 10px 15px" }}>
                {selectedTopic === 'All Solutions' ? (
                  <h3 style={{ "fontFamily": 'Open Sans' }}>
                    All Solutions</h3>) :
                  <h3 style={{ "fontFamily": 'Open Sans' }}>{selectedTopic}</h3>}
              </div>
            </Grid.Column>
            <Grid.Column floated='left' width={2}>
              {!isClient && selectedRows.length === 0 && (
                <div style={{ "margin": "15px 0px 10px 15px" }}>
                  <Checkbox toggle label='Show More Filters'
                    onChange={this.toggleShowMoreFilterOptions}
                    checked={showMoreFilters} />
                </div>
              )}
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Sidebar.Pushable as={Segment} >
          <Sidebar as={Menu} width='thin' animation='push' visible={visible} icon='labeled' vertical >
            <Menu.Item name="All Solutions" active={selectedTopic === "All Solutions"} className="All Solutions"
              onClick={this.handleItemClick.bind(this)} >
              <Icon name={selectedTopic === "All Solutions" ? 'folder open' : 'folder outline'} className="All Solutions" />All Solutions
            </Menu.Item>
            {_.map(productOptions, ({ value, label, text }) => (
              <Menu.Item key={value} name={label} className={label}
                active={selectedTopic === label} onClick={this.handleItemClick.bind(this)}>
                <Icon name={selectedTopic === label ? 'folder open' : 'folder outline'}
                  className={label} />{label}
              </Menu.Item>
            ))}
          </Sidebar>
          <Sidebar.Pusher>
            <Grid>
              <Grid.Row>
                <Grid.Column width={tableWidth}>
                  <div style={{ "marginLeft": "20px" }} >
                    <Segment basic>
                      <div style={{ "textAlign": "center" }}>
                        {success && (
                          <Message positive attached="bottom">
                            <Message.Header>Solution Update</Message.Header>
                            <p>Solution Refreshing is successful.</p>
                          </Message>
                        )}
                        {deleteSuccss && (
                          <Message positive>
                            <Message.Header>Delete Solution</Message.Header>
                            <p>Delete Solution is successful.</p>
                          </Message>
                        )}
                      </div>
                      <ReactTable
                        data={solutions}
                        noDataText="No solution is available"
                        filterable
                        defaultFilterMethod={filterMethod}
                        defaultFiltered={
                          [{
                            id: 'topic',
                            value: topic
                          }]
                        }
                        columns={[
                          {
                            accessor: "title",
                            Cell: row => (
                              <div style={{ "marginLeft": "30px" }}>
                                <Label as='a' ribbon>{row.value}</Label>
                                <ReactNbsp count={3} />
                                {(!!row.original.visibility && row.original.visibility === 'public')
                                  ? <div></div> :
                                  <Icon circular size='small' name='lock' />}
                              </div>
                            ),
                            filterMethod: (filter, row) => {
                              const query = _.escapeRegExp(filter.value);
                              if (query.length === 0) {
                                return false;
                              }
                              const regex = new RegExp(query, 'i');
                              return regex.test(row.title) || regex.test(row.content);
                            },
                            Filter: ({ filter, onChange }) => (
                              <Input type='text'
                                label={
                                  <Popup trigger={<Checkbox toggle
                                    onChange={this.toggleSearchSolutionOnBothTitleAndContent}
                                    checked={searchSolutionOnBothTitleAndContent}
                                  />} content='Search solution by title and content' />
                                }
                                labelPosition='right'
                                iconPosition='left'
                                icon={{ name: 'search', circular: false, link: false }}
                                placeholder={searchSolutionOnBothTitleAndContent ? "Search solution by title and content" : "Search solution by title"}
                                value={searchSolutionOnBothTitleAndContent ? query : (filter ? filter.value : '')}
                                onChange={event => searchSolutionOnBothTitleAndContent ? this.handleSearchBarChange(event.target.value) : onChange(event.target.value)}
                                style={{ "marginLeft": "20px", "marginRight": "150px", "width": "450px" }}
                              />
                            ),
                            maxWidth: 500,
                            width: 600
                          },
                          {
                            accessor: "status",
                            searchable: false,
                            filterable: true,
                            width: 150,
                            show: (!isClient && showMoreFilters),
                            Cell: ({ value }) => (
                              <div style={{ "marginLeft": "30px" }}>{value}</div>
                            ),
                            filterMethod: (filter, row) => {
                              if (filter.value === 'all') {
                                return true;
                              }
                              return row.status === filter.value;
                            },
                            Filter: ({ filter, onChange }) => (
                              <select
                                onChange={e => onChange(e.target.value)}
                                style={{ "width": "100%" }}
                                value={filter ? filter.value : "all"} >
                                <option value="all">Show All</option>
                                <option value="Approved">Approval</option>
                                <option value="Draft">Draft</option>
                                <option value="Rejection">Rejected</option>
                              </select>
                            )
                          },
                          {
                            accessor: "visibility",
                            searchable: false,
                            filterable: true,
                            width: 250,
                            show: (!isClient && showMoreFilters),
                            Cell: ({ value }) => (
                              <div style={{ "marginLeft": "30px" }}>
                                {value === 'public' ? "Show to Client" :
                                  (value === 'private' ? "Show to Internal Users" : "Show to Tech Services")}
                              </div>
                            ),
                            filterMethod: (filter, row) => {
                              if (filter.value === 'all') {
                                return true;
                              }
                              return row.visibility === filter.value;
                            },
                            Filter: ({ filter, onChange }) => (
                              <select
                                onChange={e => onChange(e.target.value)}
                                style={{ "width": "100%" }}
                                value={filter ? filter.value : "all"} >
                                <option value="all">Show All</option>
                                <option value="private">Show to Internal Users</option>
                                <option value="public">Show to Client</option>
                                <option value="admin">Show to Tech Services</option>
                              </select>
                            )
                          }
                        ]}
                        getTrProps={(state, rowInfo) => {
                          return {
                            onClick: (e) => {
                              if (includeElement(selectedRows, rowInfo.index)) {
                                this.setState({
                                  ...this.state, success: false, selectedRows: [],
                                  tableWidth: 12, selectedSolution: null
                                });
                              }
                              else {
                                const obj = { index: rowInfo.index, info: rowInfo.original };
                                const { solutionId, title } = rowInfo.original;
                                this.setState({ ...this.state, loading: true });
                                this.props.getSolutionByIdAndTitle(solutionId, title)
                                  .then(res => {
                                    if (res) {
                                      selectedRows = [];
                                      const solution = res.data.entities.solutions[res.data.result];
                                      selectedRows.push(obj);
                                      this.setState({
                                        ...this.state, success: false, selectedRows, tableWidth: 5, solutions,
                                        selectedSolution: solution, loading: false
                                      });
                                    }
                                  })
                                  .catch(err => {
                                    this.setState({
                                      ...this.state, success: false, loading: false, tableWidth: 12,
                                      errors: { global: err.response.data.errors.global }
                                    })
                                  });
                              }
                            },
                            style: {
                              background: (rowInfo && includeElement(selectedRows, rowInfo.index)) ?
                                '#00afec' : 'white',
                              color: (rowInfo &&
                                includeElement(selectedRows, rowInfo.index)) ?
                                'white' : 'black'
                            }
                          }
                        }}
                        defaultPageSize={20}
                        className="-striped -highlight" />
                    </Segment>
                  </div>
                </Grid.Column>
                <Grid.Column width={9}>
                  {selectedSolution && <ViewSimpleSolution solution={selectedSolution} deleteSolution={this.handleDeleteSolution} />}
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column floated='right' width={4}>
                  <Button basic color='blue' icon
                    labelPosition='left' onClick={this.refetchSolutionsList.bind(this)}>
                    <Icon name="refresh" />
                    Check for new solutions
                </Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <br /><br />
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </div>
    )
  }
}

SolutionMainPage.propTypes = {
  fetchSolutions: PropTypes.func.isRequired,
  searchSolutions: PropTypes.func.isRequired,
  getSolutionByIdAndTitle: PropTypes.func.isRequired,
  downloadAllAttachmentsBySolutionId: PropTypes.func.isRequired,
  deleteSolution: PropTypes.func.isRequired,
  fetchProducts: PropTypes.func.isRequired,
  isClient: PropTypes.bool.isRequired,
  isAdmin: PropTypes.bool.isRequired,
  products: PropTypes.arrayOf(PropTypes.string)
}

const mapStateToProps = (state) => ({
  isClient: state.user.role === 'client',
  isInternal: state.user.role === 'internal',
  isGLTech: state.user.role === 'gl_tech',
  isAdmin: state.user.role === 'admin' || state.user.role === 'manager',
  isTechSenior: state.user.role === 'tech_senior',
  products: state.user.role === 'client' ? state.user.products : [],
  topicOptions: state.products.topicOptions
})

export default connect(mapStateToProps, {
  downloadAllAttachmentsBySolutionId,
  searchSolutions,
  fetchSolutions,
  getSolutionByIdAndTitle,
  deleteSolution,
  fetchProducts
})(SolutionMainPage);
