import React from 'react';
import { Link } from 'react-router-dom';
import Footer from '../navigation/Footer';
import LogoImg from '../../img/logo.png';

const NoMatch = ({ location }) => (
  <div>
     <div className="pageWrapper">
       <header className="top-head" data-sticky="true">
          <div className="container">
             <div className="logo">
                <Link to="/"><img alt="" src={LogoImg} /></Link>
             </div>
             <div className="f-right responsive-nav">
                <nav className="top-nav nav-animate to-bottom">
                   <ul>
                      <li className="selected"><a href="/">Home</a>&nbsp;</li>
                   </ul>
                </nav>
             </div>
          </div>
       </header>
        <div id="contentWrapper">
           <div className="section">
              <div className="container">
                 <div className="row">
                    <div className="padding-vertical-30 not-found clearfix">
                       <div className="lg-not-found f-left">404<i></i></div>
                       <div className="ops">
                          <span className="main-color bold font-50">Sorry!</span><br/>
                          <span className="pg-nt-fnd">The page <code>{location.pathname}</code>
                          you are looking for could not be found.</span>
                       </div>
                       <div>
                          <p className="t-center">Contact&nbsp;&nbsp;
                             <a href="mailto:support@translations.com?subject=Info
                                request&body=Dear%20Team,%0A%0Aplease%20provide
                                %20assisstance%20for%20the%20following%20matter:%0A%0A%0AThank%20you,%0A%0A">
                             <i className="fa fa-envelope"></i>&nbsp;support@translations.com</a>.
                          </p>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
           <Footer/>
        </div>
     </div>
  </div>
)

export default NoMatch;
