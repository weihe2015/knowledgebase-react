import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Segment } from '../../utils/reactSemanticUI';
import queryString from 'query-string';
import axios from 'axios';

import SolutionForm from '../forms/SolutionForm';
import { draftSolution, getSolutionByIdAndTitle } from '../../actions/solutions';
import { validateToken, logout } from '../../actions/auth';
import { fetchProducts } from '../../actions/products';
import { TopBar, DisclaimModel, UseModel, VersionModel } from './HomePage';
import Footer from '../navigation/Footer';
import HomeHeader from '../navigation/HomeHeader';
import hdBackgroundImage from '../../img/hd_back.png';
class EditSolutionPage extends React.Component {
	state = {
		solutionId: null,
		solution: null,
		errors: {}
	}

	onSolutionSelect = solution => this.setState({ solution })

	editSolution = (solution) => {
		this.props.draftSolution(solution)
			.then(() => this.props.history.push('/dashboard'))
	}

	componentWillMount = () => {
		this.props.fetchProducts();
		const token = localStorage.knowledgebaseJWT;
		this.props.validateToken(token).then(res => {
			if (this.props.location.search.length > 0) {
				const solutionId = queryString.parse(this.props.location.search).solutionId;
				const title = queryString.parse(this.props.location.search).title;
				this.props.getSolutionByIdAndTitle(solutionId, title)
				.then(solution => {
					if (solution) {
						this.setState({
							solutionId,
							solution: solution.data.entities.solutions[solution.data.result]
						})
					}
					else {
						// TODO:
						this.setState({errors: {global: "Invalid Solution ID"}});
					}
				})
				.catch(err => console.log(err)/*this.setState({ errors: err.response.data.errors})*/)
			}
			else {
				//console.log(this.props);
			}
		})
		.catch(err => this.props.logout());
	}

  handleLogout = () => {
    // user login with TPT Auth:
    if (localStorage.knowledgebaseTPTOAuthJWT) {
      const id_token = localStorage.knowledgebaseTPTOAuthJWT;
      localStorage.removeItem('knowledgebaseTPTOAuthJWT');
      localStorage.removeItem('knowledgebaseJWT');
      delete axios.defaults.headers.common.authorization;

      const host = window.location.protocol + "//" + window.location.hostname;
      const endSessionURL = `https://sso.transperfect.com/connect/endsession?id_token_hint=${id_token}&post_logout_redirect_uri=${host}`;
      window.location = endSessionURL;
    }
    else {
      this.props.logout();
      window.location.href="/";
    }
  }

	render() {
		const { solution } = this.state;
		return (
			<div style={{"backgroundImage": `url(${hdBackgroundImage})`}}>
				<TopBar />
				<HomeHeader handleLogout={ this.handleLogout }/>
				<Segment>
					{this.state.solution && (
						<SolutionForm submit={this.editSolution} solution={solution} action={"edit"}/>
					)}
				</Segment>
				<VersionModel/>
				<DisclaimModel />
				<UseModel />
				<Footer />
			</div>
		)
	}

}

EditSolutionPage.propTypes = {
	draftSolution: PropTypes.func.isRequired,
	getSolutionByIdAndTitle: PropTypes.func.isRequired,
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	}).isRequired,
	validateToken: PropTypes.func.isRequired,
	logout: PropTypes.func.isRequired,
	fetchProducts: PropTypes.func.isRequired
}


export default connect(null, { draftSolution,
	getSolutionByIdAndTitle,
	validateToken,
	logout,
	fetchProducts })(EditSolutionPage);
