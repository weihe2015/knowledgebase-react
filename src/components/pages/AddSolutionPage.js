import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Segment, Message } from '../../utils/reactSemanticUI';
import SolutionForm from '../forms/SolutionForm';
import { createSolution,
			   fetchNewSolutionId,
				 updateSolutionByAttachment } from '../../actions/solutions';
import { validateToken, logout } from '../../actions/auth';
import DashboardFooter from '../navigation/DashboardFooter';
import TopNavigation from '../navigation/TopNavigation';

class AddSolutionPage extends React.Component {
	state = {
		solutionId: -1,
		solution: null,
		errors: {}
	}

	componentWillMount = () => {
		const token = localStorage.knowledgebaseJWT;
		this.props.validateToken(token)
			.then(res => this.props.fetchNewSolutionId()
						.then(res => this.setState({solutionId: res.data})))
			.catch(err => this.props.logout());
	}

	addSolution = (solution) => {
		// handle upload file in add solution action:
		if (solution.hasOwnProperty('tmpFiles')) {
			const files = solution.tmpFiles;
			delete solution.tmpFiles;
			this.props.createSolution(solution)
			  .then(() => {
					// update images to solution:
					let promises = [];
					files.forEach(async (file) => {
						const promise = this.props.updateSolutionByAttachment(solution, file);
						promises.push(promise);
					});
					Promise.all(promises)
						.then(data => this.props.history.push('/dashboard'))
						.catch(err => this.setState({errors: {global: err.message}}));
				})
				.catch(err => this.setState({errors: {global: err.message}}));
		}
		else {
			this.props.createSolution(solution)
				.then(() => this.props.history.push('/dashboard'));
		}
	}

	render() {
		const {solutionId, solution, errors } = this.state;
		const { isAuthenticated, isConfirmed } = this.props;
		return (
			<div>
				{ (isAuthenticated && isConfirmed) && <TopNavigation /> }
			<Segment>
				<h1>Add a new Solution</h1>
				{ (errors && errors.global) &&
					<Message negative>
						<Message.Header>Something went wrong</Message.Header>
						<p>{errors.global}</p>
					</Message>}
					<SolutionForm submit={this.addSolution} solution={solution}
				solutionId={solutionId} action={"add"}/>
				<DashboardFooter/>
			</Segment>
		</div>
		)
	}

}

AddSolutionPage.propTypes = {
	createSolution: PropTypes.func.isRequired,
	fetchNewSolutionId: PropTypes.func.isRequired,
	updateSolutionByAttachment: PropTypes.func.isRequired,
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	}).isRequired,
	validateToken: PropTypes.func.isRequired,
	logout: PropTypes.func.isRequired,
	username: PropTypes.string.isRequired,
	isAuthenticated: PropTypes.bool.isRequired,
	isConfirmed: PropTypes.bool.isRequired,
}

const mapStateToProps = (state) => ({
	isConfirmed: !!state.user.confirmed,
	isAuthenticated: !!state.user.email,
	username: state.user.fullName
})

export default connect(mapStateToProps, {createSolution,
	fetchNewSolutionId,
	updateSolutionByAttachment,
	validateToken,
	logout})(AddSolutionPage);
