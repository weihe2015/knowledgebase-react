import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Form, Button, Label, Message } from '../../utils/reactSemanticUI';
import owasp from 'owasp-password-strength-test';
import InlineError from '../messages/InlineError';

class ChangePasswordForm extends React.Component {
	state = {
		data: {
			token: this.props.token,
      currentPassword: '',
			password: '',
			passwordConfirmation: ''
		},
		loading: false,
		errors: {}
	}

	onChange = e =>
		this.setState({
			...this.state,
			data: { ...this.state.data, [e.target.name]: e.target.value }
		});

	onSubmit = e => {
		e.preventDefault();
		const errors = this.validate(this.state.data);
		this.setState({ errors });
		if (Object.keys(errors).length === 0) {
			this.setState({ loading: true });
			this.props
				.submit(this.state.data)
				.catch(err => this.setState({ errors: err.response.data.errors, loading: false }));
		}
	}

	validate = data => {
		const errors = {};
    if (!data.currentPassword) {
      errors.currentPassword = "Current Password cannot be blank.";
    }
		if (!data.password) {
      errors.password = [];
			errors.password.push("Password cannot be empty.");
		}
		else if (data.password.length < 8) {
			errors.password = [];
			errors.password.push("Password must be at least 8 characters.");
		}
    else if (data.password === data.currentPassword) {
      errors.password = [];
      errors.password.push("Password cannot be the same as current Password.");
    }
		else {
			const pwdTestResult = owasp.test(data.password);
			if (pwdTestResult.errors.length === 0) {
				delete errors.password;
			} else {
				errors.password = pwdTestResult.errors;
			}
		}
		if (data.password !== data.passwordConfirmation) {
      errors.passwordConfirmation = "Passwords must match.";
    }

		return errors;
	}

	render() {
		const { errors, data, loading } = this.state;
		return (
			<Form onSubmit={this.onSubmit} loading={loading} >
				{ errors.global && <Message negative>
						<Message.Header>Something went wrong</Message.Header>
						<p>{errors.global}</p>
					</Message>}
        <Form.Field error={!!errors && !!errors.currentPassword} required>
          <label htmlFor="currentPassword">Current Password</label>
          <input
            type="password"
            id="currentPassword"
            name="currentPassword"
            placeholder="Your current password"
            value={data.currentPassword}
            onChange={this.onChange}
          />
          { (errors && errors.currentPassword) && <InlineError text={errors.currentPassword} />}
        </Form.Field>
				<Form.Field error={!!errors && !!errors.password} required>
					<label htmlFor="password">New Password</label>
					<input
						type="password"
						id="password"
						name="password"
						placeholder="Your new password"
						value={data.password}
						onChange={this.onChange}
					/>
					{(!!errors && !!errors.password) ? _.map(errors.password, (error) =>
						(<div key={error} >
							<InlineError text={error} /><br/>
						</div>)) : <div></div>}
				</Form.Field>
				<Form.Field error={!!errors && !!errors.passwordConfirmation} required>
					<label htmlFor="passwordConfirmation">Confirm Your New Password</label>
					<input
						type="password"
						id="passwordConfirmation"
						name="passwordConfirmation"
						placeholder="Type it again, please."
						value={data.passwordConfirmation}
						onChange={this.onChange}
					/>
          { (errors && errors.passwordConfirmation) && <InlineError text={errors.passwordConfirmation} />}
				</Form.Field>
				<Label pointing>Passwords must be at least 8 characters long and must contain one of the following characters !@#$%^&amp;()</Label>
				<br /><br />
				<Button primary floated="right">Change</Button>
			</Form>
		)
	}
}

ChangePasswordForm.propTypes = {
	submit: PropTypes.func.isRequired,
	token: PropTypes.string.isRequired
}

export default ChangePasswordForm;
