import React from 'react';
import { Form, Button, Message } from '../../utils/reactSemanticUI';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import InlineError from '../messages/InlineError';

class LoginForm extends React.Component {
	state = {
		data: {
			email: '',
			password: ''
		},
		loading: false,
		errors: {}
	}

	onChange = e => this.setState({
		data: {...this.state.data, [e.target.name]: e.target.value }
	})

	onSubmit = () => {
		const { data } = this.state;
		const errors = this.validate(data);
		this.setState({errors});
		if (Object.keys(errors).length === 0) {
			this.setState({loading: true});
			this.props
			.submit(data)
			.catch(err => this.setState({ errors: err.response.data.errors, loading: false}));
		}
	}

	validate = (data) => {
		const errors = {};

		if (!data.email) {
			errors.email = "Email/Username cannot be empty.";
		}
		if (!data.password) {
			errors.password = "Password cannot be empty.";
		}
		return errors;
	}

	render() {
		const { data, errors, loading } = this.state;
		return (
			<Form onSubmit={this.onSubmit} loading={loading}>
				{ (errors && errors.global) && <Message negative>
						<Message.Header>Something went wrong</Message.Header>
						<p>{errors.global}</p>
					</Message>}
				<Form.Field required>
					<label htmlFor="email">Email or username:</label>
					<Form.Input
						error={!!errors && !!errors.email}
						type="text"
						name="email"
						placeholder="example@example.com"
						value={data.email}
						onChange={this.onChange}
					/>
				</Form.Field>
				{ (errors && errors.email) && <InlineError text={errors.email} />}

				<Form.Field required>
					<label htmlFor="password">Password:</label>
					<Form.Input
						error={!!errors && !!errors.password}
						type="password"
						id="password"
						name="password"
						placeholder="Make it secure"
						value={data.password}
						onChange={this.onChange}
					/>
				</Form.Field>
				{ (errors && errors.password) && <InlineError text={errors.password} />}
				<br/>
				<Button primary size="large">Login</Button>
				<Link to="forgotpassword"
					style={{"display": "inline-block", "marginLeft": "10px"}}>
					Forgot Password?
				</Link>
			</Form>
		);
	}
}

LoginForm.propTypes = {
	submit: PropTypes.func.isRequired
};

export default LoginForm;
