import React from 'react';
import { Button, Checkbox, Dropdown, Form, Message } from '../../utils/reactSemanticUI';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import isEmail from 'validator/lib/isEmail';
import InlineError from '../messages/InlineError';
import Select from 'react-select';
import { roleOptions } from '../common';
import { fetchProducts } from '../../actions/products'
import owasp from 'owasp-password-strength-test';
import { signup } from '../../actions/auth';

owasp.config({
	allowPassphrases: true,
	maxLength: 128,
	minLength: 4,
	minPhraseLength: 20,
	minOptionalTestsToPass: 4,
});

class SignupForm extends React.Component {
	state = {
		data: {
			firstName: '',
			lastName: '',
			username: '',
			email: '',
			role: '',
      products: [],
      SSOUser: false
		},
		success: false,
		loading: false,
		errors: {}
	}

	componentWillMount = () => {
		this.props.fetchProducts();
	}

	handleProductsOptionChange = (e, { name, value }) => {
		this.setState({
			...this.state,
			success: false,
			data: {
				...this.state.data,
				[name]: value
			}
		})
	}

	handleRoleChange = selectedOption => {
		this.setState({
			...this.state,
			data: { ...this.state.data, role: selectedOption.value }
		})
	}

	onChange = e => this.setState({
		data: { ...this.state.data, [e.target.name]: e.target.value }
  })

  onChangeIsSSOUserOption = (e) => {
    this.setState({
      ...this.state,
      data: {
        ...this.state.data,
        SSOUser: !this.state.data.SSOUser
      }
    });
  }

	onSubmit = e => {
		e.preventDefault();
		const errors = this.validate(this.state.data);
		this.setState({ errors });

		if (Object.keys(errors).length === 0) {
			this.setState({ loading: true });
			this.props.signup(this.state.data)
				.then(() => this.setState({
					...this.state, success: true, loading: false
				}))
				.catch(err =>
					this.setState({ errors: err.response.data.errors, success: false, loading: false })
				);
		}
	}

	validate = (data) => {
		const errors = {};
		if (!isEmail(data.email)) {
			errors.email = "Invalid email";
		}
		if (!data.firstName) {
			errors.firstName = "First Name cannot be empty.";
		}
		if (!data.lastName) {
			errors.lastName = "Last Name cannot be empty.";
		}
		if (!data.username) {
			errors.username = "Username cannot be empty.";
		}
		// if (!data.password) {
		// 	errors.password = [];
		// 	errors.password.push("Password cannot be empty.");
		// }
		// else {
		// 	const pwdTestResult = owasp.test(data.password);
		// 	if (pwdTestResult.errors.length === 0) {
		// 		delete errors.password;
		// 	} else {
		// 		errors.password = pwdTestResult.errors;
		// 	}
		// }
		return errors;
	}

	render() {
		const { data, errors, loading, success } = this.state;
		return (
			<div style={{ "width": "800px", "marginLeft": "150px" }}>
				<Form onSubmit={this.onSubmit} loading={loading}>
					{success && (
						<Message positive>
							<Message.Header>Add User update</Message.Header>
							<p>User with username: {data.username} has created successfully.</p>
						</Message>
					)}
					{(errors && errors.global) && (
						<Message negative>
							<Message.Header>Something went wrong</Message.Header>
							<p>{errors.global}</p>
						</Message>
					)}
					<Form.Field required >
						<label htmlFor="firstName">First Name</label>
						<Form.Input
							error={!!errors && !!errors.firstName}
							type="text"
							id="firstName"
							name="firstName"
							placeholder="First Name"
							value={data.firstName}
							onChange={this.onChange}
						/>
						{(errors && errors.firstName) && (<InlineError text={errors.firstName} />)}
					</Form.Field>

					<Form.Field required>
						<label htmlFor="firstName">Last Name</label>
						<Form.Input
							error={!!errors && !!errors.lastName}
							type="text"
							id="lastName"
							name="lastName"
							placeholder="Last Name"
							value={data.lastName}
							onChange={this.onChange}
						/>
						{(errors && errors.lastName) && <InlineError text={errors.lastName} />}
					</Form.Field>

					<Form.Field required>
						<label htmlFor="username">Username</label>
						<Form.Input
							error={!!errors && !!errors.username}
							type="text"
							id="username"
							name="username"
							placeholder="Username"
							value={data.username}
							onChange={this.onChange}
						/>
						{(errors && errors.username) && <InlineError text={errors.username} />}
					</Form.Field>

					<Form.Field required>
						<label htmlFor="role">System Role</label>
						<Select
							name="role"
							searchable={false}
							clearable={false}
							value={data.role.length > 0 ? data.role : ""}
							onChange={this.handleRoleChange.bind(this)}
							options={roleOptions}
						/>
						{(errors && errors.role) && <InlineError text={errors.role} />}
					</Form.Field>
					<Form.Field required>
						<label htmlFor="product">Select Products:</label>
						{this.props.clientProductOptions &&
							<Dropdown placeholder='Select Product(s)' fluid multiple search
								disabled={data.role !== 'client'}
								name='products'
								selection options={this.props.clientProductOptions}
								onChange={this.handleProductsOptionChange.bind(this)}
							/>}
					</Form.Field>

					<Form.Field required>
						<label htmlFor="email">Email:</label>
						<Form.Input
							error={!!errors && !!errors.email}
							type="email"
							id="email"
							name="email"
							placeholder="example@example.com"
							value={data.email}
							onChange={this.onChange}
						/>
						{(errors && errors.email) && <InlineError text={errors.email} />}
					</Form.Field>

          <Form.Field required>
            <Checkbox label="Is Single Sign On User" onChange={this.onChangeIsSSOUserOption.bind(this)}
              checked={data.SSOUser} />
          </Form.Field>

					<br />
					<Button primary floated="right">Sign Up</Button>
				</Form>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
  clientProductOptions: state.products.clientProductOptions
})

SignupForm.propTypes = {
	signup: PropTypes.func.isRequired,
	fetchProducts: PropTypes.func.isRequired
};

export default connect(mapStateToProps, { signup, fetchProducts })(SignupForm);
