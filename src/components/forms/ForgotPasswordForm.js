import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Message } from '../../utils/reactSemanticUI';
import InlineError from '../messages/InlineError';
import isEmail from 'validator/lib/isEmail';

class ForgotPasswordForm extends React.Component {
	state = {
		data: {
			email: ''
		},
		loading: false,
		errors: {}
	}

	onChange = e => 
		this.setState({
			...this.state,
			data: {...this.state.data, [e.target.name]: e.target.value}
		});

	onSubmit = e => {
		e.preventDefault();
		const errors = this.validate(this.state.data);
		this.setState({ errors });
		if (Object.keys(errors).length === 0) {
			this.setState({ loading: true });
			this.props
				.submit(this.state.data)
				.catch(err => this.setState({ errors: err.response.data.errors, loading: false })
			);
		}
	}

	validate = data => {
		const errors = {};
		if (!isEmail(data.email)) errors.email = "Invalid email";
		return errors;
	}

	render() {
		const { errors, data, loading } = this.state;

		return (
			<Form onSubmit={this.onSubmit} loading={loading}>
				{!!errors.global && <Message negative>
						<Message.Header>Reset Email Warning:</Message.Header>
						{errors.global}
					</Message>
				}
				<Form.Field >
					<label htmlFor="email">Email</label>
					<Form.Input
						error={!!errors.email}
						type="text"
						id="email"
						name="email"
						placeholder="email@email.com"
						value={data.email}
						onChange={this.onChange}
					/>
					{ errors.email && <InlineError text={errors.email} /> }
				</Form.Field>
				<Button primary floated="right">Reset Password</Button>
				<br/><br/>
			</Form>
		)
	}

}

ForgotPasswordForm.propTypes = {
	submit: PropTypes.func.isRequired
}

export default ForgotPasswordForm;