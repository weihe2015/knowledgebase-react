import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import InlineError from "../messages/InlineError";
import ReactTable from "react-table";
import {
  Form,
  Button,
  Message,
  Modal
} from "../../utils/reactSemanticUI";
import {
  fetchProducts,
  createProduct,
  modifyProduct,
  deleteProduct
} from "../../actions/products";

class ProductForm extends React.Component {
  state = {
    info: {
      productName: ""
    },
    editProductInfo: {
      _id: "",
      productName: ""
    },
    deleteProductInfo: {
      _id: "",
      productName: "",
    },
    errors: {},
    loading: false,
    showCreateProductModal: false,
    createNewProductSuccess: false,
    showEditProductModal: false,
    modifyProductSuccess: false,
    showDeleteProductModalConfirmation: false,
    deleteProductSuccess: false,
    products: []
  };

  componentWillMount = () => {
    this.props.fetchProducts().then(res => {
      this.setState({
        ...this.state,
        products: res.data.products
      });
    });
  };

  openCreateProductModal = () => {
    this.setState({
      ...this.state,
      showCreateProductModal: true,
      createNewProductSuccess: false,
      modifyProductSuccess: false,
      deleteProductSuccess: false,
    });
  };

  closeCreateProductModal = () => {
    let { errors } = this.state;
    delete errors.newProductName;
    this.setState({
      ...this.state,
      errors,
      showCreateProductModal: false
    });
  };

  onNewProductNameChange = event => {
    this.setState({
      ...this.state,
      info: {
        productName: event.target.value
      }
    });
  };

  handleCreateNewProduct = event => {
    event.preventDefault();
    let { errors, info } = this.state;
    if (info.productName.length === 0) {
      errors.newProductName = "Product name cannot be empty";
      this.setState({ ...this.state, errors });
      return;
    }
    if (Object.keys(errors).length === 0) {
      this.setState({
        ...this.state,
        loading: true,
        createNewProductSuccess: false
      });
      this.props
        .createProduct(info)
        .then(res => {
          this.setState({
            ...this.state,
            products: res.data.products,
            createNewProductSuccess: true
          });
        })
        .catch(err => {
          console.log(err);
        })
        .finally(() => {
          this.setState({
            ...this.state,
            info: {
              productName: ""
            },
            loading: false,
            showCreateProductModal: false
          });
        });
    }
  };

  openEditProductModal = (_id, productName) => () => {
    this.setState({
      ...this.state,
      editProductInfo: {
        _id,
        productName
      },
      showEditProductModal: true,
      modifyProductSuccess: false,
      createNewProductSuccess: false,
      deleteProductSuccess: false,
    });
  };

  closeEditProductModal = () => {
    let { errors } = this.state;
    delete errors.existingProductName;
    this.setState({
      ...this.state,
      errors,
      editProductInfo: {
        _id: "",
        productName: ""
      },
      showEditProductModal: false
    });
  };

  onExistingProductNameChange = event => {
    this.setState({
      ...this.state,
      editProductInfo: {
        ...this.state.editProductInfo,
        productName: event.target.value
      }
    });
  };

  handleModifyExistingProduct = event => {
    event.preventDefault();
    let { errors, editProductInfo } = this.state;
    if (editProductInfo.productName.length === 0) {
      errors.existingProductName = "Product name cannot be empty";
      this.setState({ ...this.state, errors });
      return;
    }
    if (Object.keys(errors).length === 0) {
      this.setState({
        ...this.state,
        loading: true,
        modifyProductSuccess: false
      });
      const info = {
        productName: editProductInfo.productName
      };
      this.props
        .modifyProduct(editProductInfo._id, info)
        .then(res => {
          this.setState({
            ...this.state,
            products: res.data.products,
            modifyProductSuccess: true
          });
        })
        .catch(err => {
          console.log(err);
        })
        .finally(() => {
          this.setState({
            ...this.state,
            editProductInfo: {
              _id: "",
              productName: ""
            },
            loading: false,
            showEditProductModal: false
          });
        });
    }
  };

  openDeleteProductModalConfirmation = (_id, productName) => () => {
    this.setState({
      ...this.state,
      deleteProductInfo: {
        _id,
        productName
      },
      showDeleteProductModalConfirmation: true,
      deleteProductSuccess: false,
      modifyProductSuccess: false,
      createNewProductSuccess: false
    })
  }

  closeDeleteProductModalConfirmation = () => {
    this.setState({
      ...this.state,
      deleteProductInfo: {
        _id: ""
      },
      showDeleteProductModalConfirmation: false,
      deleteProductSuccess: false
    })
  }

  handleDeleteExistingProduct = (event) => {
    event.preventDefault();
    let { deleteProductInfo } = this.state;
    this.props.deleteProduct(deleteProductInfo._id).then(res => {
      this.setState({
        ...this.state,
        products: res.data.products,
        deleteProductSuccess: true
      });
    }).catch(err => {
      console.log(err);
    }).finally(() => {
      this.setState({
        ...this.state,
        showDeleteProductModalConfirmation: false
      });
    })
  }

  render() {
    return (
      <div>
        {this.state.modifyProductSuccess && (
          <Message positive>
            <Message.Header>Modify Existing Product Update</Message.Header>
            <p>Product is modified successfully</p>
          </Message>
        )}
        {this.state.deleteProductSuccess && (
          <Message positive>
            <Message.Header>Delete Product Update</Message.Header>
            <p>Product is deleted successfully</p>
          </Message>
        )}
        <div style={{ marginBottom: "1vh" }}>
          {this.state.createNewProductSuccess && (
            <Message positive>
              <Message.Header>Add New Product Update</Message.Header>
              <p>A New Product is added successfully</p>
            </Message>
          )}
          <Button primary onClick={this.openCreateProductModal.bind(this)}>
            Add New Product
          </Button>
          <Modal
            dimmer="inverted"
            size="small"
            open={this.state.showCreateProductModal}
            onClose={this.closeCreateProductModal.bind(this)}
            style={{
              height: "30vh",
              top: "20%",
              left: "25%"
            }}
          >
            <Modal.Header>Add New Product</Modal.Header>
            <Modal.Content>
              <Form>
                <Form.Field required>
                  <label>Product Name</label>
                  <Form.Input
                    error={
                      !!this.state.errors && !!this.state.errors.newProductName
                    }
                    type="text"
                    id="productName"
                    name="productName"
                    placeholder="New Product Name"
                    value={this.state.info.productName}
                    onChange={this.onNewProductNameChange.bind(this)}
                  />
                  {this.state.errors && this.state.errors.newProductName && (
                    <InlineError text={this.state.errors.newProductName} />
                  )}
                </Form.Field>
              </Form>
            </Modal.Content>
            <Modal.Actions>
              <Button onClick={this.closeCreateProductModal.bind(this)}>
                Close
              </Button>
              <Button primary onClick={this.handleCreateNewProduct.bind(this)}>
                Create
              </Button>
            </Modal.Actions>
          </Modal>
        </div>

        <Modal
          dimmer="inverted"
          size="small"
          open={this.state.showEditProductModal}
          onClose={this.closeEditProductModal.bind(this)}
          style={{
            height: "30vh",
            top: "20%",
            left: "25%"
          }}
        >
          <Modal.Header>Edit existing Product</Modal.Header>
          <Modal.Content>
            <Form>
              <Form.Field required>
                <label>Product Name</label>
                <Form.Input
                  error={
                    !!this.state.errors &&
                    !!this.state.errors.existingProductName
                  }
                  type="text"
                  id="productName"
                  name="productName"
                  placeholder="Existing Product Name"
                  value={this.state.editProductInfo.productName}
                  onChange={this.onExistingProductNameChange.bind(this)}
                />
                {this.state.errors && this.state.errors.existingProductName && (
                  <InlineError text={this.state.errors.existingProductName} />
                )}
              </Form.Field>
            </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button onClick={this.closeEditProductModal.bind(this)}>
              Close
            </Button>
            <Button
              primary
              onClick={this.handleModifyExistingProduct.bind(this)}
            >
              Modify
            </Button>
          </Modal.Actions>
        </Modal>
        <Modal 
          dimmer="inverted"
          size="small"
          open={this.state.showDeleteProductModalConfirmation}
          onClose={this.closeDeleteProductModalConfirmation.bind(this)}
          style={{
            height: "20vh",
            top: "20%",
            left: "25%"
          }}
        >
          <Modal.Header>Delete existing Product</Modal.Header>
          <Modal.Content>
            <p>Are you sure delete product: {this.state.deleteProductInfo.productName}?</p>
          </Modal.Content>
          <Modal.Actions>
            <Button onClick={this.closeDeleteProductModalConfirmation.bind(this)}>
              Close
            </Button>
            <Button basic color="red" 
              onClick={this.handleDeleteExistingProduct.bind(this)}>
              Confirm Delete
            </Button>
          </Modal.Actions>
        </Modal>
        <ReactTable
          data={this.state.products}
          noDataText="No Product is available"
          columns={[
            {
              Header: "Product Name",
              accessor: "productName",
              sortable: true,
              width: 350
            },
            {
              Header: "Actions",
              Cell: row => {
                return (
                  <div>
                    <Button
                      basic
                      color="blue"
                      onClick={this.openEditProductModal(
                        row.original._id,
                        row.original.productName
                      )}
                    >
                      Edit
                    </Button>
                    <Button basic color="red"
                      onClick={this.openDeleteProductModalConfirmation(
                        row.original._id,
                        row.original.productName
                      )}>
                      Delete
                    </Button>
                  </div>
                );
              }
            }
          ]}
          defaultPageSize={10}
          className="-striped -highlight"
          style={{ textAlign: "center" }}
        />
      </div>
    );
  }
}

ProductForm.propTypes = {
  fetchProducts: PropTypes.func.isRequired,
  createProduct: PropTypes.func.isRequired,
  modifyProduct: PropTypes.func.isRequired,
  deleteProduct: PropTypes.func.isRequired
};

export default connect(
  null,
  { fetchProducts, createProduct, modifyProduct, deleteProduct }
)(ProductForm);
