import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Button, Comment, Form, Icon, Item, Header, Message, Tab } from '../../utils/reactSemanticUI';
import moment from 'moment';
import { updateSolution } from '../../actions/solutions';
import Task from '../../img/task.jpg';
import Setting from '../../img/setting.jpg';

class CommentForm extends React.Component {

  state = {
    solution: this.props.solution,
    loading: false,
    comment: null,
    success: false,
    errors: []
  }

  componentWillReceiveProps = props => {
    if (props.solution) {
      this.setState({
        ...this.state,
        solution: props.solution
      });
    }
  }

  onSubmit = e => {
    this.setState({ loading: true });
    let { solution } = this.state;
    const { comment } = this.state;
    const { fullName } = this.props;

    let commentObject = {
       author: fullName,
       text: comment,
       time: moment().format("YYYY-MM-DD hh:mm:ss.SSS A")
    };
    if (!solution.comments) {
       solution.comments = [];
    }
    solution.comments.unshift(commentObject);
    this.props.updateSolution(solution).then(() => {
      this.setState({solution, loading: false, success: true});
    });
  }

  handleTextChange = e => {
    this.setState({
      ...this.state,
      comment: e.target.value
    })
  }

  render() {
    const { solution, loading, success, errors } = this.state;

    const formats = {
      sameDay: '[Today] hh:mm:ss A',
      nextDay: '[Tomorrow] hh:mm:ss A',
      nextWeek: 'dddd',
      lastDay: '[Yesterday] hh:mm:ss A',
      lastWeek: '[Last] hh:mm:ss A',
      sameElse: 'YYYY-MM-DD hh:mm:ss A'
    };

    return (
      <Tab menu={{ pointing: true }} panes={[
        { menuItem: 'Comments',
            render: () => (<Tab.Pane>
              <Comment.Group>
                <Header as='h3' dividing>Comments (
                  {solution.comments ? solution.comments.length : 0}
                )</Header>
                {_.map(solution.comments, (comment) => (
                  <div key={comment.time}>
                    <Comment >
                      <Comment.Content>
                        <Comment.Author as='a'>{comment.author}</Comment.Author>
                        <Comment.Metadata>{moment(comment.time).calendar(formats)}</Comment.Metadata>
                        <Comment.Text>{comment.text}</Comment.Text>
                      </Comment.Content>
                    </Comment>
                    <hr/>
                  </div>
                ))}
              </Comment.Group>
              <Form reply onSubmit={ this.onSubmit } loading={ loading }>
                { !errors.global && (success && (<Message positive>
                    <Message.Header>Comment has added!</Message.Header>
                  </Message>)) }
                { errors.global && (<Message negative>
                  <Message.Header>Something went wrong</Message.Header>
                  <p>{errors.global}</p>
                </Message>)}
                <Form.TextArea onChange={this.handleTextChange}/>
                <Button content='Add Comment' labelPosition='left'
                  icon='edit' primary floated="right"/>
                <br/><br/>
              </Form>
            </Tab.Pane>) },
        { menuItem: 'History',
            render: () => (<Tab.Pane>
                <Item.Group divided>
                  {_.map(solution.history, (history) => (
                    <Item key={history.time}>
                      <Item.Image size='tiny' src={history.type === 'user'
                        ? Task : Setting} />
                      <Item.Content>
                        <Item.Header>{history.header}</Item.Header>
                          <Item.Meta>
                            <Icon name="lightbulb"
                              color={history.meta === 'Status: Approved' ? "green" :
                              (history.meta === 'Status: Draft' ? "orange" : "red")}>
                              </Icon>
                          {history.meta}</Item.Meta>
                        <Item.Description>{history.content}
                          {moment(history.time).calendar(formats)}</Item.Description>
                      </Item.Content>
                    </Item>
                  ))}
                </Item.Group>
            </Tab.Pane>) },
      ]} />
    )
  }
}

CommentForm.propTypes = {
	updateSolution: PropTypes.func.isRequired,
	isClient: PropTypes.bool.isRequired,
  fullName: PropTypes.string.isRequired
}

const mapStateToProps = (state) => ({
	isClient: state.user.role === 'client',
  fullName: state.user.fullName
})

export default connect(mapStateToProps, {updateSolution})(CommentForm);
