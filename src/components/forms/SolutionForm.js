import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import {
	Form, Button, Grid, Segment, Comment } from '../../utils/reactSemanticUI';
import InlineError from '../messages/InlineError';
import { dateOptions, visibilityOptions } from '../common';
import moment from 'moment';
import Select from 'react-select';
// import React Draft Wysiwyg
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import '../../css/solutionForm.css';

import 'react-select/dist/react-select.css';
import 'react-datepicker/dist/react-datepicker.css';
import SolutionInformation from '../fragment/SolutionInformation';
import UploadFileForm from './UploadFileForm';

import { insertImageToContent, fetchSolutionImage } from '../../actions/solutions';
class SolutionForm extends React.Component {

	state = {
		data: {
			_id: this.props.solution ? this.props.solution._id : null,
			solutionId: this.props.solution ? this.props.solution.solutionId : this.props.solutionId,
			title: this.props.solution ? this.props.solution.title : null,
			topic: this.props.solution ? this.props.solution.topic : null,
			content: this.props.solution ? this.props.solution.content : null,
			status: this.props.solution ? this.props.solution.status : "Draft",
			keywords: this.props.solution ? this.props.solution.keywords : "",
			visibility: this.props.solution ? this.props.solution.visibility : "private",
			expirationDate: this.props.solution ? this.props.solution.expirationDate : moment(),
			viewNumber: this.props.solution ? this.props.solution.viewNumber : 0,
			ratingUp: this.props.solution ? this.props.solution.ratingUp : 0,
			ratingDown: this.props.solution ? this.props.solution.ratingDown : 0,
			files: this.props.solution ? this.props.solution.files : [],
			history: this.props.solution ? this.props.solution.history : [],
			createdUsername: this.props.solution ? this.props.solution.createdUsername : "",
			updatedUsername: this.props.solution ? this.props.solution.updatedUsername : ""
		},
		loading: false,
		errors: {},
		dateOption: this.props.solution ?
			(moment(this.props.solution.expirationDate).isValid() ? "specific" : "never") : "never",
		expirationDate: "-",
		editorState: this.props.action === "add" ? EditorState.createEmpty() :
			EditorState.createWithContent(
				ContentState.createFromBlockArray(
					htmlToDraft(this.props.solution.content).contentBlocks
				)
			),
		editorUploadedFiles: [],
		url: ""
	}

	componentWillReceiveProps = props => {
		if (props.solution) {
			this.setState({
				data: {
					_id: props.solution._id,
					solutionId: props.solution.solutionId,
					title: props.solution.title,
					topic: props.solution.topic,
					visibility: props.solution.visibility,
					content: props.solution.content,
					status: props.solution.status,
					keywords: props.solution.keywords,
					expirationDate: props.solution.expirationDate,
					viewNumber: props.solution.viewNumber,
					ratingUp: props.solution.ratingUp,
					ratingDown: props.solution.ratingDown,
					files: props.solution.files,
					createdUsername: props.solution.createdUsername,
					updatedUsername: props.solution.updatedUsername
				},
				editorState: EditorState.createWithContent(
					ContentState.createFromBlockArray(
						htmlToDraft(props.solution.content).contentBlocks
					)
				)
			});
		}
		else if (props.solutionId !== -1) {
			this.setState({
				data: {
					...this.state.data,
					viewNumber: 0,
					ratingUp: 0,
					ratingDown: 0,
					solutionId: props.solutionId,
					status: "Draft",
					history: []
				}
			})
		}
	}

	onChange = e => this.setState({
		...this.state,
		data: { ...this.state.data, [e.target.name]: e.target.value }
	})

	onEditorStateChange = (editorState) => {
		const htmlContent = draftToHtml(convertToRaw(editorState.getCurrentContent()));
		this.setState({
			...this.state,
			data: { ...this.state.data, content: htmlContent },
			editorState
		});
	}

	// handle image upload on <Editor> component
	uploadImageCallBack = (file) => {

		// editorUploadedFiles.forEach(file => {
		// 	const reader = new FileReader();
		// 	reader.readAsDataURL(file);
		// 	reader.onloadend = () => {
		// 		console.log(reader.result);
		// 	}
		// 	//reader.readAsBinaryString(file);
		// })

		// const reader = new FileReader();
		// reader.readAsDataURL(file);
		// reader.onloadend = () => {
		// 	return new Promise((resolve, reject) => {
		// 		resolve({ data: { link: reader.result } });
		// 	});
		// }
		return new Promise((resolve, reject) => {
			let { editorUploadedFiles } = this.state;
			editorUploadedFiles.push(file);
			let url = "https://glsupport-kb.translations.com";

			// upload file to server and return an url.
			this.props.insertImageToContent(file)
				.then(res => {
					console.log(res);
					url += res.data;

					this.setState({ editorUploadedFiles, url });
					resolve({ data: { link: url } });
				})
				.catch(err => {
					console.log(err);
					reject();
				});
		});
	}

	handleDateChange = date => {
		this.setState({
			...this.state,
			data: { ...this.state.data, expirationDate: date }
		});
	}

	handleTopicChange = selectedOption => {
		this.setState({
			...this.state,
			data: { ...this.state.data, topic: selectedOption.value }
		})
	}

	handleVisibilityChange = selectedOption => {
		this.setState({
			...this.state,
			data: { ...this.state.data, visibility: selectedOption.value }
		})
	}

	handleDateOptionChange = option => {
		if (option === 'specific') {
			if (!this.state.data.expiration) {
				this.setState({
					...this.state,
					data: { ...this.state.data, expiration: moment() },
					dateOption: option.value
				});
			} else {
				this.setState({
					...this.state,
					data: { ...this.state.data },
					dateOption: option.value
				});
			}
		}
		else {
			this.setState({
				...this.state,
				data: { ...this.state.data, expirationDate: null },
				dateOption: option.value
			});
		}
	}

	onSubmit = e => {
		e.preventDefault();
		const errors = this.validate(this.state.data);
		this.setState({ errors });
		if (Object.keys(errors).length === 0) {
			this.setState({ loading: true });
			this.props.submit(this.state.data);
		}
	}

	validate = data => {
		const errors = {};
		if (!data.title) errors.title = "Solution Title cannot be blank";
		if (!data.topic) errors.authors = "Solution Topic cannot be blank";
		if (!data.content || data.content === "<p></p>") errors.pages = "Solution content cannot be blank";
		return errors;
	}

	componentWillMount = () => {
		const { data } = this.state;
		if (moment(data.expirationDate).isValid()) {
			this.setState({ dateOption: "specific" });
		}
		else {
			this.setState({ dateOption: "never" });
		}
	}

	render() {
		const { errors, data, loading, dateOption, editorState } = this.state;
		let { expirationDate } = this.state;

		if (moment(data.expirationDate).isValid()) {
			expirationDate = moment(data.expirationDate).format("YYYY-MM-DD hh:mm A");
		}
		else {
			expirationDate = "-";
		}

		return (
			<Segment>
				<Form loading={loading} style={{ "marginLeft": "20px" }}>
					<Grid columns={2} celled>
						<Grid.Row>
							<SolutionInformation solution={data} />
							<Grid.Column width={12}>
								<Form.Field error={!!errors.title} required>
									<label htmlFor="title">Solution Title</label>
									<input
										type="text"
										id="title"
										name="title"
										placeholder="Title"
										value={data.title ? data.title : ""}
										onChange={this.onChange}
									/>
									{errors.title && <InlineError text={errors.title} />}
								</Form.Field>
								<Grid columns={2} >
									<Grid.Row>
										<Grid.Column>
											<div style={{"marginLeft": "20px"}} >
											<Form.Field error={!!errors.topic} required>
												<label htmlFor="topic">Topic</label>
												<Select
													name="topic"
													searchable={false}
													clearable={false}
													value={data.topic ? data.topic : ""}
													onChange={this.handleTopicChange}
													options={this.props.topicOptions}
												/>
												{errors.topic && <InlineError text={errors.topic} />}
											</Form.Field>
											</div>
										</Grid.Column>
										<Grid.Column>
											<Form.Field required>
												<label htmlFor="visibility">Visibility</label>
												<Select
													name="visibility"
													searchable={false}
													clearable={false}
													value={data.visibility ? data.visibility : ""}
													onChange={this.handleVisibilityChange}
													options={visibilityOptions}
												/>
											</Form.Field>
										</Grid.Column>
									</Grid.Row>
								</Grid>
								<br/>
								<Form.Field error={!!errors.content} >
									<label htmlFor="topic">Content</label>
									<Editor
										editorState={editorState}
										wrapperClassName="demo-wrapper"
										editorClassName="demo-editor"
										onEditorStateChange={this.onEditorStateChange}
										toolbar={{
											image: {
												previewImage: true,
												uploadCallback: this.uploadImageCallBack,
												alt: { present: true, mandatory: false }
											}
										}}
									/>
									{errors.content && <InlineError text={errors.content} />}
								</Form.Field>

								<UploadFileForm solution={data} />

								<Form.Field >
									<label htmlFor="keywords">Keywords</label>
									<input
										type="text"
										id="keywords"
										name="keywords"
										placeholder="Keywords"
										value={data.keywords ? data.keywords : ""}
										onChange={this.onChange}
									/>
									<Comment>
										<Comment.Content>
											<Comment.Text>
												<p style={{ "color": "#a9a9a9" }}>* Keywords should be comma separated.</p>
											</Comment.Text>
											<Comment.Text>
												<p style={{ "color": "#a9a9a9" }}>
													* * Choosing a relevent keyword for a solution will improve its search capability:
													For example, Printer, toner, paper</p>
											</Comment.Text>
										</Comment.Content>
									</Comment>
								</Form.Field>

								<Form.Field>
									<label htmlFor="expiration">Expire On</label>
									<Grid celled>
										<Grid.Row centered>
											<Grid.Column width={3}>
												<Select
													name="dateTopic"
													searchable={false}
													clearable={false}
													value={dateOption}
													onChange={this.handleDateOptionChange.bind(this)}
													options={dateOptions}
												/>
											</Grid.Column>
											{(dateOption === 'specific') && (
												<Grid.Column width={6}>
													<DatePicker
														name="date"
														showTimeSelect
														timeFormat="HH:mm"
														timeIntervals={30}
														dateFormat="YYYY-MM-DD h:mm A"
														minDate={moment()}
														selected={!moment(expirationDate).isValid() ?
															moment() : moment(new Date(expirationDate))}
														onChange={this.handleDateChange.bind(this)}
													/>
												</Grid.Column>)}
										</Grid.Row>
									</Grid>
								</Form.Field>
								<Button floated="right" color='blue' size='large' onClick={this.onSubmit}>Save</Button>
								<Link to={`view?solutionId=${data.solutionId}&title=${data.title}`}>
									<Button floated="right" color='green' size='large'>Cancel</Button>
								</Link>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Form>
			</Segment>
		)
	}
}

const mapStateToProps = (state) => ({
	topicOptions: state.products.topicOptions
})

SolutionForm.propTypes = {
	submit: PropTypes.func.isRequired,
	solution: PropTypes.shape({
		solutionId: PropTypes.number.isRequired,
		title: PropTypes.string.isRequired,
		topic: PropTypes.string.isRequired,
		content: PropTypes.string.isRequired,
		status: PropTypes.string.isRequired,
		viewNumber: PropTypes.number.isRequired,
		ratingUp: PropTypes.number.isRequired,
		ratingDown: PropTypes.number.isRequired
	}),
	dateOption: PropTypes.string,
	insertImageToContent: PropTypes.func.isRequired
}

export default connect(mapStateToProps, { insertImageToContent, fetchSolutionImage })(SolutionForm);
