import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button, Icon, Message, Dimmer, Loader } from '../../utils/reactSemanticUI';
import Dropzone from 'react-dropzone';
import _ from 'lodash';
import { updateSolutionByAttachment,
		downloadAttachment } from '../../actions/solutions';
import { base64ToArrayBuffer,
		saveByteArray } from '../../utils/downloadFile';

class UploadFileForm extends React.Component {

	state = {
		solution: this.props.solution,
		files: {},
		loading: false,
		errors: {},
		uploaded: false
	}

	componentWillReceiveProps = props => {
		if (props.solution) {
			this.setState({
				...this.state,
				solution: props.solution
			})
		}
	}

	onDrop = (acceptedFiles, rejectedFiles) => {
		// acceptedFiles.forEach(file => {
		// 	const reader = new FileReader();
		// 	reader.readAsDataURL(file);
		// 	reader.onloadend = () => {
		// 		//console.log(reader.result);
		// 	}
		// 	reader.readAsBinaryString(file);
		// })
		let { solution } = this.state;
		// handle upload file in add new solution:
		if (!solution._id) {
    	solution.tmpFiles = acceptedFiles;
		}
		this.setState({files: acceptedFiles})
	}

	handleDownloadAttachment = (solutionId, filename) => {
		this.props.downloadAttachment(solutionId, filename)
			.then((res) => saveByteArray(filename, base64ToArrayBuffer(res.data.string), res.data.type));
	}

	handleSaveClick = async (event) => {
		const { files, solution } = this.state;
		this.setState({loading: true});
		if (files.length > 0) {
			let promises = [];
			files.forEach(async (file) => {
				const promise = this.props.updateSolutionByAttachment(solution, file);
				promises.push(promise);

			});
			await Promise.all(promises)
				.then((data) => {
					if (data) {
						const updatedSolution = data[data.length-1].data.entities.solutions[data[data.length-1].data.result];
						this.setState({solution: updatedSolution, files: [], loading: false, uploaded: true})
					}
					else {
						this.setState({errors: {global: "No Solution is returned."}})
					}
				})
				.catch(err => this.setState({errors: {global: err.message}, loading: false}));
		}
		else {
			await this.setState({loading: false, errors: {global: "No attachement is uploaded."}})
		}
	}

	render() {
		const { files, solution, loading, errors, uploaded } = this.state;
		return (
			<div>
				<label htmlFor="Attachments">Attachments</label>
				<br/>
				<div>
					{ !errors.global && (uploaded && (<Message positive>
							<Message.Header>Files have been successfully uploaded!</Message.Header>
						</Message>)) }
					{ errors.global && (<Message negative>
						<Message.Header>Something went wrong</Message.Header>
						<p>{errors.global}</p>
					</Message>)}
				</div>
				<br/>
				<section>
					<Dimmer inverted active={loading}>
						<Loader indeterminate>Uploading Files</Loader>
					</Dimmer>
					<div className="dropzone" >
						<Dropzone onDrop={this.onDrop.bind(this)} multiple={true}>
							<div style={{"marginLeft": "20px", "marginTop": "50px"}}>
								<Icon circular size="small" name="attach" />
								<p >Drop files or select files to upload.</p>
							</div>
						</Dropzone>
					</div>
					<br />
					<aside>
						<h4>File(s) attached:</h4>
						<ul>
							{_.map(solution.files, (file) => (
								<li key={file.name}><Link to={`/solutions/view?solutionId=${solution.solutionId}&title=${encodeURI(solution.title)}`}
									onClick={() => this.handleDownloadAttachment(solution.solutionId, file.name)}>
								{file.name} - ({file.size/1000} KB)</Link></li>
							))}
							{_.map(files, (file) => (
									<li key={file.name}>{file.name} - ({file.size/1000} KB)</li>))}
						</ul>
						<br/>
						{ solution._id &&
							(<Button color="blue" floated='right' 
								onClick={(event) => this.handleSaveClick(event)}>Save attachments</Button>)}
						<br/><br/>
					</aside>
				</section>
				<hr/>
			</div>
		)
	}
}

UploadFileForm.propTypes = {
	updateSolutionByAttachment: PropTypes.func.isRequired,
	downloadAttachment: PropTypes.func.isRequired
}

export default connect(null, {updateSolutionByAttachment, downloadAttachment})(UploadFileForm);
