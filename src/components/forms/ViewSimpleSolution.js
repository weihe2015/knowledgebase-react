import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Comment, Icon, Grid, Transition } from '../../utils/reactSemanticUI';
import moment from 'moment';
import Parser from 'html-react-parser';
import { grayColor } from '../../styles';
import ReactNbsp from 'react-nbsp';

class ViewSimpleSolution extends React.Component {
  state = {
    solution: this.props.solution,
    isSolutionDeletionConfirmPortalOpen: false
  }

  componentWillReceiveProps = props => {
    if (props.solution) {
      this.setState({ solution: props.solution });
    }
  }

  handleSolutionDeleteConfirmCancel = () => {
    this.setState({
      ...this.state,
      isSolutionDeletionConfirmPortalOpen: false
    });
  }

  handleSolutionDeleteConfirmPortalOpen = () => {
    this.setState({
      ...this.state,
      isSolutionDeletionConfirmPortalOpen: !this.state.isSolutionDeletionConfirmPortalOpen
    });
  }

  deleteSolution = e => {
    e.preventDefault();
    const { solution } = this.state;
    this.props.deleteSolution(solution);
    this.setState({
      ...this.state,
      isSolutionDeletionConfirmPortalOpen: false
    });
  }

  render() {
    const { solution, isSolutionDeletionConfirmPortalOpen } = this.state;
    const { isClient, isAdmin, isGLTech } = this.props;
    let solutionVisibilityText;
		if (!!solution.visibility && solution.visibility === 'public') {
			solutionVisibilityText = "Show to Client";
		}
		else if (!!solution.visibility && solution.visibility === 'private') {
			solutionVisibilityText = "Show to Internal Users";
		}
		else if (!!solution.visibility && solution.visibility === 'tech_senior') {
			solutionVisibilityText = "Show to Tech Services Senior";
		}
		else {
			solutionVisibilityText = "Show to Tech Services";
		}
    return (
      <div style={{ "marginTop": "30px" }}>
        <h2>{solution.title}</h2>
        <Comment>
          <Comment.Content>
            <Comment.Text>
              <label htmlFor="topic" style={grayColor}>Topic:<ReactNbsp count={3} />{solution.topic}</label>
            </Comment.Text>
            {!isClient && <div>
              <Comment.Text>
                <label htmlFor="status" style={grayColor}>
                  Status:<ReactNbsp count={3} />
                  <Icon circular size='small' name='info' color={solution.status === 'Approved' ? 'green' :
                    solution.status === 'Draft' ? 'blue' : 'red'} />
                  {solution.status}</label>
              </Comment.Text>
              <Comment.Text>
                <label htmlFor="visibility" style={grayColor}>
                  Visiblity:<ReactNbsp count={3} />
                  {solutionVisibilityText}
                </label>
              </Comment.Text>
              <Comment.Text>
                <label htmlFor="date" style={{ "color": "#a9a9a9" }}>
                  Created by:<ReactNbsp count={3} />
                  {solution.createdUsername} on <ReactNbsp count={3} />
                  {moment(solution.createdAt).format("YYYY-MM-DD hh:mm A")}
                  <ReactNbsp count={5} />
                  Last Updated by:<ReactNbsp count={3} />
                  {solution.updatedUsername} on <ReactNbsp count={3} />
                  {moment(solution.updatedAt).format("YYYY-MM-DD hh:mm A")}
                </label>
              </Comment.Text>
            </div>}
          </Comment.Content>
        </Comment>
        <hr />
        <Grid columns={1} celled>
          <Grid.Row>
            <Grid.Column>
              <div className="content-block" style={{ "marginLeft": "30px" }}>
                {Parser(solution.content)}
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <hr />
        {!isGLTech && (
          <Link to={`/solutions/view?solutionId=${solution.solutionId}&title=${solution.title}`} target="_blank">
            View Solution In Detail</Link> )}
        <ReactNbsp count={3} />
        <br /><br />
        {isAdmin && (
          <Grid columns={2}>
            <Grid.Row>
              <Grid.Column>
                <Link to={`/solutions/edit?solutionId=${solution.solutionId}&title=${solution.title}`} target="_blank">
                  <Button floated='left' size='medium' color="grey">
                    Edit Solution
                  </Button>
                </Link>
              </Grid.Column>
              <Grid.Column>
                <Button floated="right" color="pink"
                  onClick={this.handleSolutionDeleteConfirmPortalOpen.bind(this)}>
                    {!isSolutionDeletionConfirmPortalOpen ? "Delete Solution" : "Cancel Solution Deletion"}
                </Button>
                <Transition visible={isSolutionDeletionConfirmPortalOpen} animation='scale' duration={500}>
                  <Button floated='right' color='red' size='medium' onClick={this.deleteSolution.bind(this)}>
                    Confirm Solution Deletion
                  </Button>
                </Transition>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        )}
      </div>
    )
  }
}

ViewSimpleSolution.propTypes = {
  isClient: PropTypes.bool.isRequired,
  isAdmin: PropTypes.bool.isRequired,
  solution: PropTypes.shape({
    solutionId: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    topic: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    viewNumber: PropTypes.number.isRequired,
    ratingUp: PropTypes.number.isRequired,
    ratingDown: PropTypes.number.isRequired
  })
}


const mapStateToProps = (state) => ({
  isClient: state.user.role === 'client',
  isAdmin: state.user.role === 'admin' || state.user.role === 'manager' || state.user.role === 'tech_senior'
})

export default connect(mapStateToProps, {})(ViewSimpleSolution)
