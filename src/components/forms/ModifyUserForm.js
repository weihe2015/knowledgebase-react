import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Button, Checkbox, Dimmer, Dropdown,
    Icon, Input, Grid, Loader, Message } from '../../utils/reactSemanticUI';
import _ from 'lodash';
import InlineError from '../messages/InlineError';
import Select from 'react-select';
import ReactTable from "react-table";
import { fetchUserInfo, changeUserInfo,
    fetchUsersInfo, deleteUser } from '../../actions/auth';
import { fetchProducts } from '../../actions/products';
import { roleOptions, notificationOptions } from '../common';

import {
  includeElement, removeElement, filterMethod
} from '../../utils/helper';

class ModifyUserForm extends React.Component {
  state = {
      data: {
        _id: '',
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        role: '',
        products: [],
        notification: undefined,
        SSOUser: false
      },
      users: [],
      selectedRow: [],
      loading: false,
      userInfoLoading: false,
      userInfoFetchSuccess: false,
      success: false,
      deleteUserSuccess: false,
      errors: {}
  }

  componentWillMount = () => {
    this.props.fetchProducts();
    const { token } = this.props;
    this.props.fetchUsersInfo(token).then(res => {
      this.setState({
        ...this.state,
        users: res.users
      });
    });
  }

  onChange = e => this.setState({
    data: {...this.state.data, deleteUserSuccess: false, success: false,
      [e.target.name]: e.target.value }
	})

  handleDropdownOptionChange = (e, { name, value }) => {
    this.setState({
      ...this.state,
      success: false,
      data: {
        ...this.state.data,
        [name]: value
      }
    })
  }

  handleNotficationOptionChange = (e, {name, value}) => {
    this.setState({
      ...this.state,
      success: false,
      data: {
        ...this.state.data,
        [name]: value === 'true' ? true : false
      }
    })
  }

  fetchUserInfo = e => {
    let { data } = this.state;
    if (data.email.length === 0) {
      this.setState({errors: { email: "Username/Email cannot be empty." }});
    }
    else {
      this.setState({...this.state, userInfoLoading: true, userInfoFetchSuccess: false});
      this.props.fetchUserInfo(data.email).then(res => {
        this.setState({
          ...this.state,
          userInfoLoading: false,
          userInfoFetchSuccess: true,
          data: {
            ...this.state.data,
            _id: res.user._id,
            email: res.user.email,
            role: res.user.role,
            SSOUser: res.user.SSOUser,
            firstName: res.user.fullName.split(' ')[0],
            lastName: res.user.fullName.split(' ')[1]
          }
        })
      }).catch (err => {
        this.setState({
          ...this.state,
          userInfoLoading: false,
          errors: {email : err.response.data.errors.global},
          data: {
            ...this.state.data,
            email: ''
          }
        })
      });
    }
  }

  onChangeIsSSOUserOption = (e) => {
    this.setState({
      ...this.state,
      data: {
        ...this.state.data,
        password: !this.state.data.SSOUser ? '' : this.state.data.password,
        SSOUser: !this.state.data.SSOUser
      }
    });
  }

  handleRoleChange = selectedOption => {
    this.setState({
      ...this.state,
      userInfoFetchSuccess: false,
      data: { ...this.state.data, role: selectedOption.value }
    })
  }

  onHandleUserDeletion = e => {
    e.preventDefault();
    const { data } = this.state;
    let errors = this.validate(data);
    const currEmail = this.props.email;
    if (currEmail === data.email) {
      errors.email = "User cannot delete themselves";
      this.setState({ errors, userInfoFetchSuccess: false });
      return;
    }
    const confirmMessage = `Are you sure to delete user ${data.firstName} ${data.lastName} with email ${data.email}? `;
    if (!window.confirm(confirmMessage)) {
      return;
    }
    this.setState({ errors, userInfoFetchSuccess: false });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true, deleteUserSuccess: false });
      const { token } = this.props;
      this.props.deleteUser(data._id).then(res => {
        this.props.fetchUsersInfo(token).then(res1 => {
          this.setState({ loading: false, deleteUserSuccess: true, users: res1.users,
            data: {
              _id: '',
              firstName: '',
              lastName: '',
              email: '',
              password: '',
              role: '',
              products: [],
              notification: undefined,
              SSOUser: false
            }
          });
        })
        .catch(err => {
          this.setState({ errors: {global: err.response.data.errors.global},
            loading: false, deleteUserSuccess: false });
        });
      })
      .catch(err => {
        this.setState({ errors: {global: err.response.data.errors.global},
          loading: false, deleteUserSuccess: false });
      });
    }
  }

  onSubmit = e => {
    e.preventDefault();
    let { data } = this.state;
    const errors = this.validate(data);
    this.setState({ errors, userInfoFetchSuccess: false });

    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      const { token } = this.props;
      // Reset password field if this user is SSO user:
      if (data.SSOUser) {
        data.password = '';
      }
      this.props.changeUserInfo(data).then(res => {
        this.props.fetchUsersInfo(token).then(res1 => {
          this.setState({ loading: false, success: true, users: res1.users });
        })
        .catch(err => {
          this.setState({ errors: {global: err.response.data.errors.global},
            loading: false, success: false });
        });
      })
      .catch(err => {
        this.setState({ errors: {global: err.response.data.errors.global},
          loading: false, success: false });
      });
    }
  }
  validate = (data) => {
    const errors = {};
    if (!data.email) {
      errors.email = "Username/email cannot be empty";
    }
    return errors;
  }

  render() {
    const { errors, data, users, loading, selectedRow,
      userInfoLoading, userInfoFetchSuccess, success, deleteUserSuccess } = this.state;

    return (
      <Grid columns={2} >
        <Grid.Row>
          <Grid.Column width={10}>
            <h3 style={{"textAlign": "center"}}>All Users Info</h3>
            <ReactTable
              data={users}
              noDataText="No User is existed"
              filterable
              defaultFilterMethod={filterMethod}
              defaultFiltered={
                [{
                  id: 'role',
                  value: 'all'
                }]}
              columns={[
                {
                  Header: "Email",
                  accessor: "email",
                  Filter: ({ filter, onChange }) => (
                    <Input type='text'
                      placeholder="Search User by email"
                      value={filter ? filter.value : ''}
                      onChange={event => onChange(event.target.value)}
                      style={{ "width": "250px" }}
                    />
                  ),
                  width: 250
                },
                {
                  Header: "Username",
                  accessor: "username",
                  Filter: ({ filter, onChange }) => (
                    <Input type='text'
                      placeholder="Search User by username"
                      value={filter ? filter.value : ''}
                      onChange={event => onChange(event.target.value)}
                      style={{ "width": "250px" }}
                    />
                  ),
                  width: 250
                },
                {
                  Header: "First Name",
                  accessor: "firstName",
                  sortable: true,
                  filterable: false,
                  width: 150
                },
                {
                  Header: "Last Name",
                  accessor: "lastName",
                  filterable: false,
                  sortable: true,
                  width: 150
                },
                {
                  Header: "Role",
                  accessor: "role",
                  width: 250,
                  filterable: true,
                  filterMethod: (filter, row) => {
                    if (filter.value === 'all') {
                      return true;
                    }
                    return row.role === filter.value;
                  },
                  Filter: ({ filter, onChange }) => (
                    <select onChange={e => onChange(e.target.value)}
                      style={{ "width": "100%" }}
                      value={filter ? filter.value : "all"} >
                      <option value="all">Show All</option>
                      {_.map(roleOptions, (roleOption) => (
                        <option key={roleOption.value} value={roleOption.value}>
                          {roleOption.label}
                        </option>
                      ))}
                    </select>
                  )
                },
                {
                  Header: "Is Confirmed",
                  accessor: "confirmed",
                  Cell: props => (<span className='number'>{props.value.toString()}</span>),
                  filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                      return true;
                    }
                    return row.confirmed.toString() === filter.value.toLowerCase();
                  },
                  Filter: ({ filter, onChange }) => (
                    <select
                      onChange={event => {
                        onChange(event.target.value);
                      }}
                      style={{ "width": "100%" }}
                      value={filter ? filter.value : "all"}>
                      <option value="all">Show All</option>
                      <option key="True" value="true">True</option>
                      <option key="False" value="false">False</option>
                    </select>
                  )
                },
                {
                  Header: "Notification",
                  accessor: "notification",
                  Cell: props => (<span className='number'>{props.value.toString()}</span>),
                  filterable: false,
                  sortable: true,
                  width: 100
                },
                {
                  Header: 'Is SSO User',
                  accessor: 'SSOUser',
                  Cell: props => (<span className='boolean'>{props.value.toString()}</span>),
                  filterable: false,
                  sortable: true,
                  width: 100
                }
              ]}
              getTrProps={(state, rowInfo) => {
                return {
                  onClick: (e) => {
                    if (includeElement(selectedRow, rowInfo.index)) {
                      removeElement(selectedRow, rowInfo.index);
                      this.setState({...this.state, selectedRow: [],
                        data: {
                          _id: '',
                          firstName: '',
                          lastName: '',
                          email: '',
                          password: '',
                          role: '',
                          notification: false,
                          SSOUser: false
                        }
                      });
                    }
                    else {
                      const obj = { index: rowInfo.index, info: rowInfo.original };
                      let newSelectedRow = [];
                      newSelectedRow.push(obj);
                      this.setState({...this.state, selectedRow: newSelectedRow,
                        data: {
                          _id: rowInfo.original._id,
                          firstName: rowInfo.original.firstName,
                          lastName: rowInfo.original.lastName,
                          email: rowInfo.original.email,
                          password: '',
                          role: rowInfo.original.role,
                          products: !!rowInfo.original.products ? rowInfo.original.products: [],
                          notification: !!rowInfo.original.notification ? rowInfo.original.notification : false,
                          SSOUser: rowInfo.original.SSOUser,
                        }
                      });
                    }
                  },
                  style: {
                    background: (rowInfo &&
                      includeElement(selectedRow, rowInfo.index)) ?
                        '#00afec' : 'white',
                    color: (rowInfo &&
                      includeElement(selectedRow, rowInfo.index)) ?
                        'white' : 'black'
                  }
                }
              }}
              defaultPageSize={15}
              className="-striped -highlight"
              style={{ "textAlign": "center" }}
            />
          </Grid.Column>
          <Grid.Column floated='right' width={6}>
            <h3 style={{"textAlign": "center"}}>Change User Info</h3>
            <Form >
              { (errors && errors.global) && <Message negative>
                  <Message.Header>Something went wrong</Message.Header>
                  <p>{errors.global}</p>
                </Message>}
              { (success) && <Message positive>
                  <Message.Header>User Info changed update:</Message.Header>
                  <p>User Info changed successfully!</p>
                </Message>
              }
              { (userInfoFetchSuccess) && <Message positive>
                  <Message.Header>User Info fetched update:</Message.Header>
                  <p>User Info fetched successfully!</p>
                </Message>
              }
              { (deleteUserSuccess) && (
                <Message positive>
                  <Message.Header>User deleted update:</Message.Header>
                  <p>User has deleted successfully!</p>
                </Message>
              )}

              <Dimmer inverted active={userInfoLoading}>
                <Loader indeterminate>Loading User Info, please wait...</Loader>
              </Dimmer>
              <Dimmer inverted active={loading}>
                <Loader indeterminate>Changing User Info, please wait...</Loader>
              </Dimmer>

              <Form.Field required>
                <label htmlFor="email">Email or username:</label>
                <Form.Input
                  error={!!errors && !!errors.email}
                  type="text"
                  name="email"
                  placeholder="Type username or email to fetch User Info"
                  value={data.email}
                  onChange={this.onChange}
                />
                { (errors && errors.email) && <InlineError text={errors.email} />}
              </Form.Field>
              <Button basic color='teal' icon
                labelPosition='left' onClick={this.fetchUserInfo.bind(this)}>
                <Icon name="refresh" />
                Fetch User Info
              </Button>
              <br /><br />
              <Form.Field required>
                <label htmlFor="role">System Role</label>
                <Select
                  name="role"
                  searchable={false}
                  value={data.role.length > 0 ? data.role : ""}
                  onChange={this.handleRoleChange.bind(this)}
                  options={roleOptions}
                />
                { (errors && errors.role) && <InlineError text={errors.role} /> }
              </Form.Field>
              {data.notification !== undefined && (
                <Form.Field>
                  <label htmlFor="notification">Email Notification</label>
                  <Dropdown
                    name="notification"
                    disabled={!data.role || data.role !== 'admin'}
                    selection
                    value={data.notification ? 'true' : 'false'}
                    options={notificationOptions}
                    onChange={this.handleNotficationOptionChange.bind(this)}
                  />
                  { (errors && errors.notification) && <InlineError text={errors.notification} /> }
                </Form.Field>
              )}
              <Form.Field >
                <label htmlFor="firstName">First Name</label>
                <Form.Input
                  error={!!errors && !!errors.firstName}
                  type="text"
                  id="firstName"
                  name="firstName"
                  placeholder="First Name"
                  value={data.firstName}
                  onChange={this.onChange}
                />
                { (errors && errors.firstName) && <InlineError text={errors.firstName} />}
              </Form.Field>

              <Form.Field >
                <label htmlFor="firstName">Last Name</label>
                <Form.Input
                  error={!!errors && !!errors.lastName}
                  type="text"
                  id="lastName"
                  name="lastName"
                  placeholder="Last Name"
                  value={data.lastName}
                  onChange={this.onChange}
                />
                { (errors && errors.lastName) && <InlineError text={errors.lastName} />}
              </Form.Field>

              <Form.Field >
                <label htmlFor="password">Password</label>
                <Form.Input
                  error={!!errors && !!errors.password}
                  type="password"
                  id="password"
                  name="password"
                  value={data.password}
                  onChange={this.onChange}
                  disabled={data.SSOUser}
                />
                { (errors && errors.password) ? _.map(errors.password, (error) =>
                  (<div key={error}>
                    <InlineError text={error} /><br/>
                  </div>)) : <div></div>}
              </Form.Field>
              <Form.Field required>
                  <label htmlFor="product">Select Products:</label>
                  {/* Render the dropdown onlywhen there are options in this.props.clientProductOptions */}
                  {this.props.clientProductOptions &&
                    <Dropdown placeholder='Select Product(s)'
                      fluid multiple search
                      name='products'
                      disabled={data.role !== 'client'}
                      selection
                      options={this.props.clientProductOptions}
                      value={data.products}
                      onChange={this.handleDropdownOptionChange.bind(this)}
                    />
                  }
              </Form.Field>
              <Form.Field required>
                <Checkbox label="Is Single Sign On User" onChange={this.onChangeIsSSOUserOption.bind(this)}
                  checked={data.SSOUser} />
              </Form.Field>
              <br/>
              <Button floated="left" color="red" size="large" onClick={this.onHandleUserDeletion}>
                Delete User
              </Button>
              <Button primary floated="right" onClick={this.onSubmit} size="large">
                Change
              </Button>
              <br/><br/>
            </Form>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }

}

const mapStateToProps = (state) => ({
  token: state.user.token,
  email: state.user.email,
  clientProductOptions: state.products.clientProductOptions
})

ModifyUserForm.propTypes = {
  fetchUserInfo: PropTypes.func.isRequired,
  changeUserInfo: PropTypes.func.isRequired,
  fetchUsersInfo: PropTypes.func.isRequired,
  deleteUser: PropTypes.func.isRequired,
  fetchProducts: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired
}

export default connect(mapStateToProps, {fetchUserInfo,
  changeUserInfo, fetchUsersInfo, deleteUser, fetchProducts})(ModifyUserForm);
