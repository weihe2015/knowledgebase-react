import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Form, Button, Label, Message } from '../../utils/reactSemanticUI';
import owasp from 'owasp-password-strength-test';
import InlineError from '../messages/InlineError';

class ResetPasswordForm extends React.Component {
	state = {
		data: {
			token: this.props.token,
			password: '',
			passwordConfirmation: ''
		},
		loading: false,
		errors: {}
	}

	onChange = e =>
		this.setState({
			...this.state,
			data: { ...this.state.data, [e.target.name]: e.target.value }
		});

	onSubmit = e => {
		e.preventDefault();
		const errors = this.validate(this.state.data);
		this.setState({ errors });
		if (Object.keys(errors).length === 0) {
			this.setState({ loading: true });
			this.props
				.submit(this.state.data)
				.catch(err => this.setState({ errors: err.response.data.errors, loading: false }));
		}
	}

	validate = data => {
		let errors = {};
		if (!data.password) {
			errors.password = "Password cannot be empty.";
		}
		else if (data.password.length < 8) {
			errors.password = [];
			errors.password.push("Password must be at least 8 characters.");
		}
		else {
			const pwdTestResult = owasp.test(data.password);
			if (pwdTestResult.errors.length === 0) {
				delete errors.password;
			} else {
				errors.password = pwdTestResult.errors;
			}
		}
		if (data.password !== data.passwordConfirmation) {
			errors.passwordConfirmation = "Passwords must match.";
		}
		return errors;
	}

	render() {
		const { errors, data, loading } = this.state;
		return (
			<Form onSubmit={this.onSubmit} loading={loading} >
				{ errors.global && <Message negative>
						<Message.Header>Something went wrong</Message.Header>
						<p>{errors.global}</p>
					</Message>}
				<Form.Field error={!!errors.password} required>
					<label htmlFor="password">New Password</label>
					<input
						type="password"
						id="password"
						name="password"
						placeholder="Your new Password"
						value={data.password}
						onChange={this.onChange}
					/>
					{(errors && errors.password) ? _.map(errors.password, (error) =>
						(<div key={error}>
							<InlineError text={error} /><br/>
						</div>)) : <div></div>}
				</Form.Field>
				<Form.Field error={!!errors.passwordConfirmation} required>
					<label htmlFor="passwordConfirmation">Confirm Your New Password</label>
					<input
						type="password"
						id="passwordConfirmation"
						name="passwordConfirmation"
						placeholder="type it again, please."
						value={data.passwordConfirmation}
						onChange={this.onChange}
					/>
          { (errors && errors.passwordConfirmation) && <InlineError text={errors.passwordConfirmation} />}
				</Form.Field>
				<Label pointing>Passwords must be at least 8 characters long and must contain one of the following characters !@#$%^&amp;()</Label>
				<br /><br />
				<Button primary>Reset</Button>
			</Form>
		)
	}
}

ResetPasswordForm.propTypes = {
	submit: PropTypes.func.isRequired,
	token: PropTypes.string.isRequired
}

export default ResetPasswordForm;
