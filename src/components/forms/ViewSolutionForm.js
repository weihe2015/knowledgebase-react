import React from 'react';
import ReactNbsp from 'react-nbsp';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import {
	Button, Comment, Grid, Icon, Header, Message, Portal,
	Segment, Transition, TextArea
} from '../../utils/reactSemanticUI';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import Parser from 'html-react-parser';

import SolutionInformation from '../fragment/SolutionInformation';
import { grayColor } from '../../styles';
import {
	approveSolution, rejectSolution, deleteSolution,
	incViewNumber, incRatingUp, incRatingDown, hasUserVotedSolution
} from '../../actions/solutions';
import UploadFileForm from './UploadFileForm';
import CommentForm from './CommentForm';

class ViewSolutionForm extends React.Component {

	state = {
		solution: this.props.solution,
		hasVoted: false,
		rating: true,
		errors: {},
		isClient: this.props.isClient,
		isAdmin: this.props.isAdmin,
		solutionApprovalUpdate: false,
		solutionRejectUpdate: false,
		visible: false,
		approvalPortalOpen: false,
		rejectionPortalOpen: false,
		approvalComment: "",
		rejectionComment: "",
		deleteSuccess: false
	}

	componentWillMount = () => {
		// check if this user has voted previously:
		const { email } = this.props;
		const { solution } = this.state;
		this.props.hasUserVotedSolution(solution.solutionId, email)
			.then((res) => {
				if (res.data.status) {
					this.setState({...this.state, hasVoted: true})
				}
			})
			.catch(err => console.log(err));
	}

	componentDidMount = () => {
		const { solution } = this.state;
		this.props.incViewNumber(solution)
			.then((res) => this.setState({ solution: res.data }))
			.catch(err => this.setState({ errors: err.response.data.errors }));
	}

	componentWillUnmount = () => {
		this.setState({ solution: null });
	}

	handleApprovalPortalOpen = () => {
		this.setState({approvalPortalOpen: !this.state.approvalPortalOpen});
	}

	handleRejectionPortalOpen = () => {
	 this.setState({rejectionPortalOpen: !this.state.rejectionPortalOpen});
	}

	handleApprovalSolutionComment = e => {
		this.setState({...this.state, approvalComment: e.target.value});
	}

	handleRejectSolutionComment = e => {
		this.setState({...this.state, rejectionComment: e.target.value});
	}

	handleSolutionApproval = () => {
		const { approvalComment } = this.state;
		const updatedSolution = this.state.solution;
		this.props.approveSolution(updatedSolution, approvalComment)
			.then(res => this.setState({
				solution: res.data,
				approvalComment: "", rejectionComment: "",
				approvalPortalOpen: false, rejectionPortalOpen: false,
				solutionApprovalUpdate: true, solutionRejectUpdate: false
			}));
	}

	handleSolutionReject = () => {
		const { rejectionComment } = this.state;
		const updatedSolution = this.state.solution;
		this.props.rejectSolution(updatedSolution, rejectionComment)
			.then(res => this.setState({
				solution: res.data,
				approvalComment: "", rejectionComment: "",
				approvalPortalOpen: false, rejectionPortalOpen: false,
				solutionApprovalUpdate: false, solutionRejectUpdate: true
		}));
	}

	handleRateUp = (e) => {
		const { solution } = this.state;
		const { email } = this.props;
		this.props.incRatingUp(solution.solutionId, email)
			.then((res) => this.setState({ solution: res.data, rating: false }))
			.catch(err => this.setState({ errors: err.response.data.errors }));
	}

	handleRateDown = (e) => {
		const { solution } = this.state;
		const { email } = this.props;
		this.props.incRatingDown(solution.solutionId, email)
			.then((res) => this.setState({ solution: res.data, rating: false }))
			.catch(err => this.setState({ errors: err.response.data.errors }));
	}

	toggleConfirmDeleteButton = e => {
		e.preventDefault();
		this.setState({ ...this.state, success: false, deleteSuccess: false, visible: !this.state.visible });
	}

	deleteSolution = e => {
		e.preventDefault();
		const { solution } = this.state;
		this.props.deleteSolution(solution)
			.then((res) => {
				this.setState({ ...this.state, success: false, visible: !this.state.visible, deleteSuccess: true });
			})
			.catch(err => this.setState({
				...this.state, success: false, deleteSuccess: false, errors: err.response.data.errors
			}))
	}

	render() {
		const { solution, errors, rating, isClient, isAdmin, visible, deleteSuccess,
			approvalPortalOpen, rejectionPortalOpen, hasVoted,
			solutionApprovalUpdate, solutionRejectUpdate } = this.state;
		const status = solution.status;

		if (deleteSuccess) {
			return (<Redirect to='/dashboard' />)
		}
		return (
			<Segment>
				{errors.global && <Message negative>
					<Message.Header>Something went wrong</Message.Header>
					<p>{errors.global}</p>
				</Message>}
				<Grid columns={2} celled>
					<Grid.Row>
						<Grid.Column width={3}>
							{solutionApprovalUpdate && (
								<Message positive>
									<Message.Header>Solution Update</Message.Header>
									<p>This solution has been approved!</p>
								</Message>
							)}
							{solutionRejectUpdate && (
								<Message negative>
									<Message.Header>Solution Update</Message.Header>
									<p>This solution has been rejected!</p>
								</Message>
							)}
							{(isAdmin && status === 'Draft') && (
								<div>
									<Button loading={solutionApprovalUpdate}
										size="medium" color='green' onClick={this.handleApprovalPortalOpen}>Approve</Button>
									<Button loading={solutionRejectUpdate}
										size="medium" color='red' onClick={this.handleRejectionPortalOpen}>Reject</Button>
								</div>
							)}
						</Grid.Column>
						{isAdmin && (
							<Grid.Column>
								<Link to={`/solutions/edit?solutionId=${solution.solutionId}&title=${solution.title}`}>
									<Button floated="left" color='teal' size='medium'>Edit Solution</Button>
								</Link>
								<Button floated='left' color='pink' size='medium' onClick={this.toggleConfirmDeleteButton.bind(this)}>
									Delete Solution
								</Button>
								<Transition visible={visible} animation='scale' duration={500}>
									<Button floated='left' color='red' size='medium' onClick={this.deleteSolution.bind(this)}>
										Confirm Delete
									</Button>
								</Transition>
							</Grid.Column>
						)}
					</Grid.Row>
					<Portal onClose={this.handleApprovalPortalOpen} open={approvalPortalOpen}>
						<Segment style={{ left: '40%', position: 'fixed', top: '30%', zIndex: 1000 }}>
							<Header>Approve Comment</Header>
							<TextArea placeholder='Tell us more about approving this solution' style={{ minHeight: 150 }}
							onChange={this.handleApprovalSolutionComment} />
							<Button floated="right" color="blue" onClick={this.handleSolutionApproval}>Approve</Button>
							<Button floated="right" onClick={this.handleApprovalPortalOpen}>Cancel</Button>
						</Segment>
					</Portal>
					<Portal onClose={this.handleRejectionPortalOpen} open={rejectionPortalOpen}>
						<Segment style={{ left: '40%', position: 'fixed', top: '30%', zIndex: 1000 }}>
							<Header>Reject Comment</Header>
							<TextArea placeholder='Tell us more about rejecting this solution' style={{ minHeight: 150 }}
								onChange={this.handleRejectSolutionComment}/>
							<Button floated="right" color="red" onClick={this.handleSolutionReject}>Reject</Button>
							<Button floated="right" onClick={this.handleRejectionPortalOpen}>Cancel</Button>
						</Segment>
					</Portal>
					<Grid.Row>
						<Grid.Column width={3}>
							<SolutionInformation solution={solution} />
						</Grid.Column>
						<Grid.Column width={10}>
							<h2>{solution.title}</h2>
							<Comment>
								<Comment.Content>
									<Comment.Text>
										<label htmlFor="topic" style={grayColor}>Topic:<ReactNbsp count={3} />
											<Link to={`/dashboard?topic=${solution.topic}`}>{solution.topic}</Link></label>
									</Comment.Text>
									{isAdmin && (
									<Comment.Text>
										<label htmlFor="date" style={{ "color": "#a9a9a9" }}>
											Created by:<ReactNbsp count={3} />
											{solution.createdUsername} on <ReactNbsp count={3} />
											{moment(solution.createdAt).format("YYYY-MM-DD hh:mm A")}
											<ReactNbsp count={5} />
											Last Updated by:<ReactNbsp count={3} />
											{solution.updatedUsername} on <ReactNbsp count={3} />
											{moment(solution.updatedAt).format("YYYY-MM-DD hh:mm A")}
										</label>
									</Comment.Text>
									)}
								</Comment.Content>
							</Comment>
							<hr />
							<div className="content-block" style={{"marginLeft":"30px"}}>
								{Parser(solution.content)}
							</div>
							<hr />
							{!isClient &&
								<UploadFileForm solution={solution} />
							}
							{hasVoted ? (
								(<div><p>
									<Icon circular size="small" name="checkmark" color={'green'} inverted={true} />
										You have voted this solution previously!
									</p></div>)
							): ((rating) ? (
								<div>
									<p>Was this Solution helpful?</p>
									<Button
										color='green'
										content='Like'
										icon='thumbs outline up'
										label={{ basic: true, color: 'green', pointing: 'left' }}
										onClick={this.handleRateUp}
									/>
									<ReactNbsp count={8} />
									<Button
										color='red'
										content='Dislike'
										icon='thumbs outline down'
										label={{ basic: true, color: 'red', pointing: 'left' }}
										onClick={this.handleRateDown}
									/>
								</div>
							) : (<div><p>
								<Icon circular size="small" name="checkmark" color={'green'} inverted={true} />
								Thank you for your feedback!
								</p></div>))}
							<hr />
							<CommentForm solution={solution} />
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</Segment>
		)
	}
}

ViewSolutionForm.propTypes = {
	approveSolution: PropTypes.func.isRequired,
	rejectSolution: PropTypes.func.isRequired,
	hasUserVotedSolution: PropTypes.func.isRequired,
	incViewNumber: PropTypes.func.isRequired,
	incRatingUp: PropTypes.func.isRequired,
	incRatingDown: PropTypes.func.isRequired,
	isClient: PropTypes.bool.isRequired,
	isAdmin: PropTypes.bool.isRequired,
	username: PropTypes.string.isRequired,
	email: PropTypes.string.isRequired,
	deleteSolution: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
	isClient: state.user.role === 'client',
	isAdmin: state.user.role === 'admin' || state.user.role === 'manager' || state.user.role === 'tech_senior',
	username: state.user.fullName,
	email: state.user.email
})

export default connect(mapStateToProps,
	{
		approveSolution, rejectSolution, deleteSolution,
		incViewNumber, incRatingUp, incRatingDown, hasUserVotedSolution
	})(ViewSolutionForm);
