import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { Form, Dropdown } from '../../utils/reactSemanticUI';

class SearchSolutionForm extends React.Component {
	state = {
		query: '',
		loading: false,
		options: [],
		solutions: {}
	}

	onSearchChange = (e, data) => {
		clearTimeout(this.timer);
		this.setState({
			query: data
		});
		this.timer = setTimeout(this.fetchOptions, 1000);
	}

	onChange = (e, data) => {
		this.setState({ query: data.value });
		this.props.onSolutionSelect(this.state.solutions[data.value]);
	}

	fetchOptions = () => {
		if (!this.state.query)
			return;
		this.setState({ loading: true });
		axios.get(`/api/solutions/search?q=${this.state.query}`)
			.then(res => res.data.solutions)
			.then(solutions => {
				const options = [];
				const solutionsHash = {};
				solutions.forEach(solution => {
					solutionsHash[solution.solutionId] = solution;
					options.push({
						key: solution.solutionId,
						value: solution.solutionId,
						text: solution.title
					})
				});
				this.setState({ loading: false, options, solutions: solutionsHash});
			})
	}

	render() {
		return (
			<Form>
				<Form.Field>
					<Dropdown
						search
						fluid
						placeholder="Search for a solution by title"
						value={this.state.query}
						onSearchChange={this.onSearchChange}
						options={this.state.options}
						loading={this.state.loading}
						onChange={this.onChange}
					/>
				</Form.Field>
			</Form>
		)
	}

}

SearchSolutionForm.propTypes = {
	onSolutionSelect: PropTypes.func.isRequired
}

export default SearchSolutionForm;
