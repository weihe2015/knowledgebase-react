export const dateOptions = [
	{value: 'never', label: 'Never'},
	{value: 'specific', label: 'Specific Date'}
];

export const roleOptions = [
	{
		value: 'admin',
		label: 'Admin User',
		text: 'Admin User'
	},
	{
		value: 'internal',
		label: 'Internal User',
		text: 'Internal User'
	},
	{
		value: 'client',
		label: 'Client User',
		text: 'Client User'
	},
	{
		value: 'gl_tech',
		label: 'GL Tech User',
		text: 'GL Tech User'
	},
	{
		value: 'tech_senior',
		label: 'GL Tech Senior User',
		text: 'GL Tech Senior User'
	},
];

export const notificationOptions = [
	{
		value: 'true',
		key: 'Enable',
		text: 'Enable'
	},
	{
		value: 'false',
		key: 'Disable',
		text: 'Disable'
	}
]

export const visibilityOptions = [
	{
		value: 'public',
		label: 'Show to client',
		text: 'Show to client'
	},
	{
		value: 'private',
		label: 'Show to Internal Users',
		text: 'Show to Internal Users'
	},
	{
		value: 'admin',
		label: 'Show to Tech Services',
		text: 'Show to Tech Services'
	},
	{
		value: 'tech_senior',
		label: "Show to Tech Services Senior",
		text: "Show to Tech Services Senior"
	}
]

export const managerEmails = [
	'ksomasundaram@translations.com', 'vsimecek@translations.com'
]

export function includeElement(array, idx) {
  for (let i = 0; i < array.length; ++i) {
    if (array[i].index === idx) {
      return true;
    }
  }
  return false;
}

export function removeElement(array, idx) {
  return array.filter(e => e.index !== idx);
}

export function filterMethod(filter, row, column) {
  const id = filter.pivotId || filter.id;
  return row[id] !== undefined ?
      String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase())
        : true;
}