import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Dropdown, Image, Icon } from '../../utils/reactSemanticUI';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import gravatarUrl from 'gravatar-url';
import { logout } from '../../actions/auth';
import { allSolutionsSelector } from '../../reducers/solutions';
import SupportCenter from '../../img/gl_techservices_logo2.png';

const TopNavigation = ({ user, logout, isClient, isAdmin }) => (
	<Menu secondary pointing>
		<Menu.Item>
			<Link to="/" onClick={() => this.props.history.push('/')}>
				<Image src={SupportCenter} size="small"/>
			</Link>
		</Menu.Item>
		<Menu.Item as={Link} to="/dashboard"
			onClick={() => this.props.history.push('/dashboard')}>Dashboard</Menu.Item>
		{!isClient && (
		<Menu.Item as={Link} to="/solutions/new">Add New Solution</Menu.Item>
	  )}
		{!isClient && (
		<Menu.Item as={Link} to="/solutions/edit">Edit Solution</Menu.Item>
		)}
		<Menu.Menu position="right" style={{"marginTop": "20px", "marginRight": "50px" }}>
			<Dropdown fluid
				trigger={<div>
					<Image avatar src={gravatarUrl(user.email)} />
						<span>{user.fullName}</span>
					</div>}>
				<Dropdown.Menu style={{"width": "150px"}}>
					<Dropdown.Item>
						<Icon name='setting' size='large'>&nbsp;&nbsp;
							<Link to="/changePassword">Change Password</Link>
						</Icon>
					</Dropdown.Item>
					{isAdmin && (
						<Dropdown.Item>
							<Icon name='add user' size='large'>&nbsp;&nbsp;
								<Link to="/signup">Add User</Link>
							</Icon>
						</Dropdown.Item>
					)}
					{isAdmin && (
							<Dropdown.Item>
								<Icon name="edit" size='large'>&nbsp;&nbsp;
									<Link to="/modifyuser">Modify User</Link>
								</Icon>
							</Dropdown.Item>
					)}
					<Dropdown.Item >
						<Icon name='sign out' size='large' >&nbsp;&nbsp;
						<Link to="/" onClick={() => {
							logout().then(this.props.history.push('/'));
						}}>Logout</Link>
						</Icon>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>
		</Menu.Menu>
	</Menu>
);

TopNavigation.propTypes = {
	user: PropTypes.shape({
		email: PropTypes.string.isRequired
	}).isRequired,
	hasSolution: PropTypes.bool.isRequired,
	isClient: PropTypes.bool.isRequired,
	isAdmin: PropTypes.bool.isRequired,
	logout: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
	user: state.user,
	hasSolution: allSolutionsSelector(state).length > 0,
	isClient: state.user.role === 'client',
	isAdmin: state.user.role === 'admin' || tate.user.role === 'manager' || state.user.role === 'tech_senior'
})

export default connect(mapStateToProps, { logout })(TopNavigation);
