import React from 'react'

const DashboardFooter = () => (
  <footer>
    <div className="footer-top" style={{"textAlign": "center"}}>
      © Copyright <b className="main-color">TransPerfect</b> {new Date().getFullYear()}. All rights
      reserved.
    </div>
  </footer>
)

export default DashboardFooter;
