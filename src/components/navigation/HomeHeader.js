import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon, Label } from '../../utils/reactSemanticUI';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import KnowledgeBaseImg from '../../img/GLKB.png';

const HomeHeader = ({ handleLogout, isAuthenticated, isAdmin, isClient, fullName }) => (
  <header className="top-head" data-sticky="true">
  <div className="container" style={{"width": "1800px"}}>
    <div className="logo">
      <img alt="KnowledgeBaseSystem&trade;" src={KnowledgeBaseImg} width="350px"/>
    </div>
    {!isAuthenticated && (<div className="f-left responsive-nav" style={{"marginLeft": "200px"}}></div>)}
    <div className="f-left responsive-nav" style={{"marginLeft": "100px"}}>
      <nav className="top-nav nav-animate to-bottom">
        <ul>
          {isAuthenticated ? (<li><Link to="/dashboard">Dashboard</Link>Knowledge System</li>) :
            (<li></li>) }
          {(isAuthenticated) && (
            <li><Link to="#">Settings</Link>User &amp; Settings
              <ul>
                <li><Link to="/changePassword"><Icon name="setting"/>Change Password</Link></li>
                {(isAdmin && !isClient) && (
                  <li><Link to="/signup"><Icon name="setting"/>Add User</Link></li>)}
                {(isAdmin && !isClient) && (
                  <li><Link to="/modifyuser"><Icon name="setting"/>Modify User</Link></li>)}
                {(isAdmin && !isClient) && (
                  <li><Link to="/products"><Icon name="setting"/>Modify Product</Link></li>
                )}
              </ul>
            </li>
          )}
          <li><Link to="#help">Help</Link>Guides &amp; FAQ
            <ul>
              <li><a href="mailto:support@translations.com?subject=TransPerfect |
                Knowledge Base Support request &body=Dear%20Support%20Team,%0A%0APlease%20provide%20assistance%20for%20the%20following:
                %0A%0A%0AApplication: KnowledgeBase%0ARequestor%27s%20Name:%0ARequestor%27s%20Email%20address:%0ASupport
                %20Issue%20%28please%20be%20specific%20and%20include%20screen%20shots%20as%20needed%29:%0A%0A%0AThank%20you,">
                <i className="fa fa-support"></i>Support Request</a></li>
            </ul>
            </li>
            <li><Link to="#contact">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Link>&nbsp;&nbsp;</li>
            {isAuthenticated && (<li>
              <Button color="blue" onClick={handleLogout}>Logout</Button></li>)}
          </ul>
        </nav>
        {(fullName && fullName.length > 0) && (
        <div className="f-right">
          <div style={{"marginTop": "30px", "marginLeft": "50px"}}>
            <Label basic size="large">
              Hello
              <Label.Detail>{fullName}!</Label.Detail>
            </Label>
          </div>
        </div>
        )}
    </div>
    </div>
</header>
)

HomeHeader.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  isAdmin: PropTypes.bool.isRequired,
  isClient: PropTypes.bool.isRequired,
  fullName: PropTypes.string
}

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.user.email,
  fullName: state.user.fullName,
  isClient: state.user.role === 'client',
  isAdmin: state.user.role === 'admin' || state.user.role === 'manager' || state.user.role === 'tech_senior'
})

export default connect(mapStateToProps, { })(HomeHeader);