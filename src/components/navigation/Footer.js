import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../../actions/auth';

const divBackground = {
	background: 'transparent'
};

const Footer = ({ logout, isAuthenticated }) => (
	<footer id="footWrapper">
		<div className="footer-top main-bg">
			<div className="container">
				<div className="row">
					<div className="f-left">
						<span className="font-20 uppercase"><span
							className="light-text bolder"><i className="fa fa-cog fa-spin"></i>
							GlobalLink</span> &ndash; The world's friendliest multilingual CMS</span>
					</div>
					<div className="f-right">
						<a href="http://www.translations.com/globallink/products/globallink_enterprise.html"
							className="alter-bg block-link">More details</a>
					</div>
				</div>
			</div>
		</div>
		<div className="footer-middle">
			<div className="container">
				<div className="row">
					<div className="col-md-4 first">
						<h3>Main Menu</h3>
						<ul className="menu-widget">
							<li><Link to="/dashboard">Dashboard</Link></li>
							{!isAuthenticated && (<li><Link to="/login">Login</Link></li>)}
						</ul>
					</div>
					<div className="col-md-4 contact-widget" style={divBackground}>
						<h3>Legal</h3>
						<ul className="details">
							<li><Link to="#" data-toggle="modal"
								data-target="#disclaimermodal"><i
									className="fa fa-bookmark shape sm"></i><span>Disclaimer</span></Link></li>
							<li><Link to="#" data-toggle="modal" data-target="#toumodal"><i
								className="fa fa-legal shape sm"></i><span>Terms of Use</span></Link></li>
						</ul>
					</div>
					<div className="col-md-4 last contact-widget">
						<h3>Contact Us</h3>
						<ul className="details">
							<li>
								<i className="fa fa-globe shape sm"></i>
								<span>Boulder, New York, Barcelona, Singapore, Pune, Shenyang</span>
							</li>
							<li><i className="fa fa-map-marker shape sm"></i><span><a href="mailto:support@translations.com">GlobalLink Technology Services Team</a></span>
							</li>
						</ul>
					</div>

				</div>
			</div>
		</div>
		<div className="footer-bottom">
			<div className="container">
				<div className="row">
					<div className="copyrights col-md-5 first">
							© Copyright <b className="main-color">TransPerfect</b> {new Date().getFullYear()}. All rights reserved.
					</div>
					<div className="col-md-7 last">
						<ul className="footer-menu f-right">
							<li><a href="http://www.translations.com/about-us">About
								Us</a></li>
							<li>|</li>
							<li><a href="http://www.transperfect.com/about/privacy.html">Privacy</a></li>
							<li>|</li>
							<li><a href="http://www.translations.com/about/global-group">Global
								Group</a></li>
						</ul>
					</div>

				</div>
			</div>
		</div>
	</footer>

);

Footer.propTypes = {
	isAuthenticated: PropTypes.bool.isRequired
}

const mapStateToProps = (state) => ({
	isAuthenticated: !!state.user.email
})

export default connect(mapStateToProps, { logout })(Footer);
