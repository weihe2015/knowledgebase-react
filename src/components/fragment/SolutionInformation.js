import React from 'react';
import { connect } from 'react-redux';
import { Grid, Icon } from 'semantic-ui-react';
import moment from 'moment';
import ReactNbsp from 'react-nbsp'

class SolutionInformation extends React.Component {
	state = {
		solution: null
	}

	componentWillMount = () => {
		this.setState({ solution: this.props.solution });
	}

	componentWillReceiveProps = props => {
		this.setState({ solution: props.solution });
	}

	componentWillUnmount = () => {
		this.setState({ solution: null });
	}

	render() {
		const { solution } = this.state;
		const { isClient } = this.props;
		let solutionVisibilityText;
		if (!!solution.visibility && solution.visibility === 'public') {
			solutionVisibilityText = "Show to Client";
		}
		else if (!!solution.visibility && solution.visibility === 'private') {
			solutionVisibilityText = "Show to Internal Users";
		}
		else if (!!solution.visibility && solution.visibility === 'tech_senior') {
			solutionVisibilityText = "Show to Tech Services Senior";
		}
		else {
			solutionVisibilityText = "Show to Tech Services";
		}
		return (
			<Grid.Column width={3}>
				<label>Solution Information</label>
				<hr />
				Solution ID:<ReactNbsp count={3} />
				{solution.solutionId !== -1 ? solution.solutionId : "loading"}
				<br /><br />
				{!isClient && (<div>
					<Icon circular size='small' name='info' color={solution.status === 'Approved' ? 'green' :
						solution.status === 'Draft' ? 'blue' : 'red'} />
					Status:<ReactNbsp count={3} />{solution.status}
					<br /><br />
					Visibility:<ReactNbsp count={3} />
					{solutionVisibilityText}
					<br /><br />
					Expires on:<ReactNbsp count={3} />
					{!solution.expirationDate ? "Never" :
						moment(solution.expirationDate).format("YYYY-MM-DD hh:mm A")}</div>)}
				<br /><br />
				<Icon circular size="small" name="eye" color={'blue'}></Icon>Views:<ReactNbsp count={6} />{solution.viewNumber}
				<br /><br />
				<Icon circular size="small" name="thumbs outline up" color={'green'}></Icon>Rating Up:
					<ReactNbsp count={6} />{solution.ratingUp}
				<br /><br />
				<Icon circular size="small" name="thumbs outline down" color={'red'}></Icon>Rating Down:
					<ReactNbsp count={6} />{solution.ratingDown}
				<br /><br />
				<Icon circular size="small" name="comments"></Icon>Comment:<ReactNbsp count={6} />
				{solution.comments ? solution.comments.length : 0}
				<br /><br />
				{!isClient && (<div>
					<label>Keywords</label>
					<hr />
					{solution.keywords}

					<br /><br />
					<label>Notification</label>
					<hr />
				</div>)}
			</Grid.Column>
		)
	}

}

const mapStateToProps = (state) => ({
	isClient: state.user.role === 'client'
})

export default connect(mapStateToProps, {})(SolutionInformation);
