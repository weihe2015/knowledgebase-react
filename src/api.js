import axios from "axios";
import request from 'superagent-bluebird-promise';
import CryptoJS from "crypto-js";

export default {
	user: {
		login: data =>
			axios.post('/api/v1/auth/login',
			{
				"ciphertext" :
				CryptoJS.AES.encrypt(JSON.stringify(data),'cryptojs_secret').toString()
			})
			.then(res => res.data.user),
		oauthLogin: (data) =>
			axios.post('/api/v1/auth/oauthlogin', {
				"ciphertext" :
				CryptoJS.AES.encrypt(JSON.stringify(data),'cryptojs_secret').toString()
			})
			.then(res => res.data.user),
		oauth: () =>
			axios.get('/api/v1/auth/oauth').then(res => res.data),
		signup: data =>
			axios.post('/api/v1/auth/user',
			{
				"ciphertext" :
				CryptoJS.AES.encrypt(JSON.stringify(data), 'cryptojs_secret').toString()
			})
			.then(res => res.data.user),
		confirm: token =>
			axios.put('/api/v1/auth/user/token', { token }).then(res => res.data.user),
		resetPasswordRequest: email =>
			axios.get(`/api/v1/auth/user/password?em=${email}`),
		validateToken: token =>
			axios.get(`/api/v1/auth/user/token/${token}`)
				.then(res => res.data),
		resetPassword: data =>
			axios.post('/api/v1/auth/user/password', {
				"ciphertext" :
				CryptoJS.AES.encrypt(JSON.stringify(data),'cryptojs_secret').toString()
			}),
		changePassword: data =>
		  axios.put('/api/v1/auth/user/password', {
				'ciphertext':
				CryptoJS.AES.encrypt(JSON.stringify(data),'cryptojs_secret').toString()
			}),
		fetchUsersInfo: (token) =>
		  axios.get(`/api/v1/auth/users?t=${token}`)
				.then(res => res.data),
		fetchUserInfo: email =>
			axios.get(`/api/v1/auth/user?em=${email}`).then(res => res.data.user),
		changeUserInfo: data =>
		 axios.put(`/api/v1/auth/user/${data._id}`,
		 {
			 'ciphertext':
			 CryptoJS.AES.encrypt(JSON.stringify(data),'cryptojs_secret').toString()
		 }).then(res => res.data),
		 deleteUser: userId =>
			axios.delete(`/api/v1/auth/user/${userId}`).then(res => res.data)
	},
	solutionCounter: {
		getNewId: () =>
			axios.get('/api/solutionCounter/solutionId')
				.then(res => res.data.counter)
	},
	solutions: {
		fetchAll: (userType) =>
			axios.get(`/api/solutions?user=${userType}`)
				.then(res => res.data.solutions),
		searchSolution: (query) =>
			axios.get(`/api/solutions?q=${query}`)
			  .then(res => res.data.solutions),
		create: solution =>
			axios.post('/api/solutions', {solution})
				.then(res => res.data.solution),
		incViewNumber: solution =>
		  axios.put(`/api/solutions/${solution.solutionId}/viewnumber/`)
				.then(res => res.data.solution),
		hasUserVoted: (solutionId, email) =>
			axios.get(`/api/solutions/${solutionId}/rating/?email=${email}`)
			  .then(res => res.data),
	  incRatingUp: (solutionId, email) =>
			axios.put(`/api/solutions/${solutionId}/ratingup/?email=${email}`)
				.then(res => res.data.solution),
		incRatingDown: (solutionId, email) =>
			axios.put(`/api/solutions/${solutionId}/ratingdown/?email=${email}`)
				.then(res => res.data.solution),
		solutionApproval: (solution, approvalComment) =>
		  axios.put(`/api/solutions/${solution.solutionId}/approval`, {solution, approvalComment})
			  .then(res => res.data.solution),
	  solutionRejection: (solution, rejectionComment) =>
			axios.put(`/api/solutions/${solution.solutionId}/rejection`, {solution, rejectionComment})
				.then(res => res.data.solution),
		solutionDeletion: solution =>
			axios.delete(`/api/solutions/${solution.solutionId}`)
				.then(res => res.data),
	  solutionDraft: solution =>
			axios.put(`/api/solutions/${solution.solutionId}/draft`, {solution})
				.then(res => res.data.solution),
		update: solution =>
			axios.put(`/api/solutions/${solution.solutionId}`, {solution})
				.then(res => res.data.solution),
		updateSolutionByAttachment: (solution, file) => {
			const token = `Bearer ${localStorage.knowledgebaseJWT}`;
			const req = request.put(`/api/solutions/${solution.solutionId}/attachment`)
				.set({ Authorization: token })
				.attach("uploadFile", file)
				.promise()
				.then(res => JSON.parse(res.text).solution);
			return req;
		},
		insertImageToContent: (file) => {
			const token = `Bearer ${localStorage.knowledgebaseJWT}`;
			const req = request.post('/api/solutions/image')
				.set({ Authorization: token })
				.attach("uploadFile", file)
				.promise()
				.then(res => JSON.parse(res.text).url);
			return req;
		},
		downloadAttachment: (solutionId, filename) =>
			axios.get(`/api/solutions/${solutionId}/attachment?filename=${filename}`)
				.then(res => res.data),
		downloadAllAttachmentsBySolutionId: (solutionId) =>
			axios.get(`/api/solutions/${solutionId}/attachments`)
			.then(res => res.data),
		getSolutionByIdAndTitle: (solutionId, title) =>
			axios.get(`/api/solutions/${solutionId}/?t=${title}`)
				.then(res => res.data.solution),
		getSolutionsByCategory: solutionCategory =>
			axios.get(`/api/solutions/category/${solutionCategory}`)
				.then(res => res.data.solutions),
		getInlineImageByName: imageName =>
			axios.get(`/inlineImages/?n=${imageName}`)
				.then(res => res.data)
	},
	products: {
		fetchProducts: () =>
			axios.get(`/api/v1/products/`).then(res => res.data),
		createProduct: (data) =>
			axios.post(`/api/v1/products/`, {
				'ciphertext':
				CryptoJS.AES.encrypt(JSON.stringify(data),'cryptojs_secret').toString()
			}).then(res => res.data),
		modifyProduct: (productId, data) =>
			axios.put(`/api/v1/products/${productId}`, {
				'ciphertext':
				CryptoJS.AES.encrypt(JSON.stringify(data),'cryptojs_secret').toString()
			}).then(res => res.data),
		deleteProduct: (productId) =>
			axios.delete(`/api/v1/products/${productId}`).then(res => res.data)
	}
};
