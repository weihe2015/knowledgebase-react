import { combineReducers } from 'redux';
import { reducer as oidcReducer } from 'redux-oidc';

import user from './reducers/user';
import solutions from './reducers/solutions';
import products from './reducers/products';

export default combineReducers({
	user,
	solutions,
	products,
	oidc: oidcReducer
});