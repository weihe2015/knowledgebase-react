import { USER_LOGGED_IN, USER_OAUTH_URL_FETCH, USER_LOGGED_OUT, USER_SIGN_UP, USER_CONFIRMED, USER_DELETED,
	USER_INFO_CHANGED, USER_INFO_FETCHED, USERS_INFO_FETCHED } from '../types';
import api from '../api';
import setAuthorizationHeader from '../utils/setAuthorizationHeader';

export const userLoggedIn = (user) => ({
	type: USER_LOGGED_IN,
	user
})

export const userOauthUrlFetched = (data) => ({
	type: USER_OAUTH_URL_FETCH,
	data
})

export const userLoggedOut = () => ({
	type: USER_LOGGED_OUT
})

export const userInfoFetched = (user) => ({
	type: USER_INFO_FETCHED,
	user
})

export const userInfoChanged = (user) => ({
	type: USER_INFO_CHANGED,
	user
})

export const userDeleted = (user) => ({
	type: USER_DELETED,
	user
})

export const usersInfoFetched = (users) => ({
	type: USERS_INFO_FETCHED,
	users
})

export const userSignup = () => ({
	type: USER_SIGN_UP
})

export const userConfirmed = () => ({
	type: USER_CONFIRMED
})

export const login = credentials => dispatch =>
	api.user.login(credentials).then(user => {
		localStorage.setItem('knowledgebaseJWT', user.token);
		setAuthorizationHeader(user.token);
		dispatch(userLoggedIn(user));
	});

export const loginWithOAuth2 = credentials => dispatch =>
	api.user.oauthLogin(credentials).then(user => {
    localStorage.setItem('knowledgebaseJWT', user.token);
    setAuthorizationHeader(user.token);
    dispatch(userLoggedIn(user));
  });

export const fetchOAuthUrl = () => dispatch =>
  api.user.oauth().then(res => dispatch(userOauthUrlFetched(res)))

export const logout = () => dispatch => {
  localStorage.removeItem('knowledgebaseJWT');
	setAuthorizationHeader();
  dispatch(userLoggedOut());
}

export const signup = credentials => dispatch =>
	api.user.signup(credentials)
	.then(user => {
		//localStorage.setItem('knowledgebaseJWT', user.token);
		dispatch(userSignup());
	});

export const confirm = token => dispatch =>
	api.user.confirm(token)
	.then(user => {
		localStorage.removeItem('knowledgebaseJWT');
		localStorage.setItem('knowledgebaseJWT', user.token);
		setAuthorizationHeader(user.token);
		dispatch(userLoggedIn(user));
	});

export const resetPasswordRequest = ({ email }) => () =>
	api.user.resetPasswordRequest(email);

export const validateToken = token => () =>
	api.user.validateToken(token);

export const resetPassword = data => () =>
	api.user.resetPassword(data);

export const changePassword = data => () =>
  api.user.changePassword(data);

export const fetchUserInfo = (user) => dispatch =>
	api.user.fetchUserInfo(user).then(res => dispatch(userInfoFetched(res)));

export const changeUserInfo = (user) => dispatch =>
	api.user.changeUserInfo(user).then(res => dispatch(userInfoChanged(res)));

export const fetchUsersInfo = (token) => dispatch =>
	api.user.fetchUsersInfo(token).then(res => dispatch(usersInfoFetched(res)));

export const deleteUser = (user) => dispatch =>
	api.user.deleteUser(user).then(res => dispatch(userDeleted(res)))
