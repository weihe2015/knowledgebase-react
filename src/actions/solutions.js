import { normalize } from 'normalizr';
import { SOLUTIONS_FETCH,
	   SOLUTIONS_SEARCH,
		 SOLUTION_CREATED,
		 SOLUTION_APPROVAL,
		 SOLUTION_DELETED,
		 SOLUTION_REJECTION,
		 SOLUTION_DRAFT,
		 SOLUTION_INC_VIEW_NUMBER,
		 SOLUTION_INC_RATINGUP,
		 SOLUTION_INC_RATINGDOWN,
		 SOLUTION_UPDATED,
		 SOLUTION_UPDATED_BY_ATTACHMENT,
		 SOLUTION_GETBYID,
		 SOLUTION_FETCH_BY_CATEGORY,
		 SOLUTION_DOWNLOAD_ATTACHMENT,
		 NEW_SOLUTION_ID_FETCHED,
		 SOLUTION_DOWNLOAD_ALL_ATTACHMENTS_BY_SOLUTIONID,
		 SOLUTION_INSERT_IMAGE_TO_CONTENT,
		 SOLUTION_IMAGE_FETCH,
		 SOLUTION_HAS_USER_VOTED
		} from '../types';
import api from '../api';
import { solutionSchema } from '../schemas';

const solutionsFetched = data => ({
	type: SOLUTIONS_FETCH,
	data
});

const solutionsSearch = (data) => ({
	type: SOLUTIONS_SEARCH,
	data
})

const solutionsFetchedByCategory = data => ({
	type: SOLUTION_FETCH_BY_CATEGORY,
	data
});

const solutionCreated = data => ({
	type: SOLUTION_CREATED,
	data
});

const solutionApproval = data => ({
	type: SOLUTION_APPROVAL,
	data
});

const solutionRejection = data => ({
	type: SOLUTION_REJECTION,
	data
});

const solutionDeletion = data => ({
	type: SOLUTION_DELETED,
	data
});

const solutionDraft = data => ({
	type: SOLUTION_DRAFT,
	data
})

const solutionIncViewNumber = data => ({
	type: SOLUTION_INC_VIEW_NUMBER,
	data
})

const solutionHasUserVoted = data => ({
	type: SOLUTION_HAS_USER_VOTED,
	data
})

const solutionIncRatingUp = data => ({
	type: SOLUTION_INC_RATINGUP,
	data
})

const solutionIncRatingDown = data => ({
	type: SOLUTION_INC_RATINGDOWN,
	data
})

const solutionUpdated = data => ({
	type: SOLUTION_UPDATED,
	data
})

const solutionUpdatedByAttachment = data => ({
	type: SOLUTION_UPDATED_BY_ATTACHMENT,
	data
})

const solutionGotByID = data => ({
	type: SOLUTION_GETBYID,
	data
})

const solutionInsertImageToContent = data => ({
	type: SOLUTION_INSERT_IMAGE_TO_CONTENT,
	data
})

const solutionDowloadAttachment = data => ({
	type: SOLUTION_DOWNLOAD_ATTACHMENT,
	data
})

const solutionAllDownloadAttachmentsBySolutionId = data => ({
	type: SOLUTION_DOWNLOAD_ALL_ATTACHMENTS_BY_SOLUTIONID,
	data
})

const newSolutionIdFetched = data => ({
	type: NEW_SOLUTION_ID_FETCHED,
	data
})

const solutionImageFetched = data => ({
	type: SOLUTION_IMAGE_FETCH,
	data
})

export const fetchNewSolutionId = () => dispatch =>
	api.solutionCounter
		.getNewId()
		.then(counter => dispatch(newSolutionIdFetched(counter)))

export const fetchSolutions = (userType) => dispatch =>
	api.solutions
		.fetchAll(userType)
		.then(solutions => dispatch(solutionsFetched(normalize(solutions, [solutionSchema]))))

export const searchSolutions = (query) => dispatch =>
	 api.solutions.searchSolution(query)
	 	.then(solutions => dispatch(solutionsSearch(normalize(solutions, [solutionSchema]))))

export const createSolution = data => dispatch =>
	api.solutions
		.create(data)
		.then(solution => dispatch(solutionCreated(normalize(solution, solutionSchema))))

export const approveSolution = (data, approvalComment) => dispatch =>
  api.solutions
	  .solutionApproval(data, approvalComment)
		.then(solution => dispatch(solutionApproval(solution)))

export const rejectSolution = (data, rejectionComment) => dispatch =>
  api.solutions
	  .solutionRejection(data, rejectionComment)
		.then(solution => dispatch(solutionRejection(solution)))

export const deleteSolution = data => dispatch =>
	api.solutions
		.solutionDeletion(data)
		.then(res => dispatch(solutionDeletion(res)))

export const draftSolution = data => dispatch =>
  api.solutions
	  .solutionDraft(data)
		.then(solution => dispatch(solutionDraft(solution)))

export const incViewNumber = data => dispatch =>
  api.solutions
	  .incViewNumber(data)
		.then(solution => dispatch(solutionIncViewNumber(solution)))

export const hasUserVotedSolution = (solutionId, email) => dispatch =>
 api.solutions.hasUserVoted(solutionId, email)
		.then(data => dispatch(solutionHasUserVoted(data)))
		
export const incRatingUp = (solutionId, email) => dispatch =>
  api.solutions
	  .incRatingUp(solutionId, email)
		.then(solution => dispatch(solutionIncRatingUp(solution)))

export const incRatingDown = (solutionId, email) => dispatch =>
  api.solutions
	  .incRatingDown(solutionId, email)
		.then(solution => dispatch(solutionIncRatingDown(solution)))

export const updateSolution = data => dispatch =>
	api.solutions
		.update(data)
		.then(solution => dispatch(solutionUpdated(solution)))

export const updateSolutionByAttachment = (data, file) => dispatch =>
	api.solutions
		.updateSolutionByAttachment(data, file)
		.then(solution => dispatch(solutionUpdatedByAttachment(normalize(solution, solutionSchema))))

export const insertImageToContent = (data) => dispatch =>
	api.solutions
		.insertImageToContent(data)
		.then(res => dispatch(solutionInsertImageToContent(res)))

export const downloadAttachment = (data, filename) => dispatch =>
	api.solutions
		.downloadAttachment(data, filename)
		.then(solution => dispatch(solutionDowloadAttachment(solution)))

export const downloadAllAttachmentsBySolutionId = (solutionId) => dispatch =>
	api.solutions.downloadAllAttachmentsBySolutionId(solutionId)
		.then(data => dispatch(solutionAllDownloadAttachmentsBySolutionId(data)))

export const getSolutionByIdAndTitle = (solutionId, title) => dispatch =>
	api.solutions
		.getSolutionByIdAndTitle(solutionId, title)
		.then(solution => dispatch(solutionGotByID(normalize(solution, solutionSchema))))

export const fetchSolutionsByCategory = viewName => dispatch =>
	api.solutions
		.getSolutionsByCategory(viewName)
		.then(solutions => dispatch(solutionsFetchedByCategory(normalize(solutions, [solutionSchema]))))

export const fetchSolutionImage = imageName => dispatch =>
	api.solutions.getInlineImageByName(imageName)
		.then(res => dispatch(solutionImageFetched(res)))
