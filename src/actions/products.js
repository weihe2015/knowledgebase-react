import { PRODUCT_FETCHED, PRODUCT_CREATED, PRODUCT_MODIFIED, PRODUCT_DELETED } from '../types';
import api from '../api';

const productFetched = (data) => ({
  type: PRODUCT_FETCHED,
  data
})

const productCreated = (data) => ({
  type: PRODUCT_CREATED,
  data
});

const productModified = (data) => ({
  type: PRODUCT_MODIFIED,
  data
});

const productDeleted = (data) => ({
  type: PRODUCT_DELETED,
  data
});

export const fetchProducts = () => dispatch =>
  api.products.fetchProducts()
    .then(products => dispatch(productFetched(products)));

export const createProduct = (data) => dispatch =>
  api.products.createProduct(data)
    .then(res => dispatch(productCreated(res)))

export const modifyProduct = (productId, data) => dispatch =>
  api.products.modifyProduct(productId, data)
    .then(res => dispatch(productModified(res)))

export const deleteProduct = (productId) => dispatch =>
  api.products.deleteProduct(productId)
    .then(res => dispatch(productDeleted(res)))


