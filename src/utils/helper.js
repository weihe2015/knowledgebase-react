export function includeElement(array, idx) {
  for (let i = 0; i < array.length; ++i) {
    if (array[i].index === idx) {
      return true;
    }
  }
  return false;
}

export function removeElement(array, idx) {
  return array.filter(e => e.index !== idx);
}

export function filterMethod(filter, row, column) {
  const id = filter.pivotId || filter.id;
  return row[id] !== undefined ?
      String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase())
        : true;
}
