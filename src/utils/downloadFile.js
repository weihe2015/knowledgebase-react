export function base64ToArrayBuffer(base64) {
		const binaryString = window.atob(base64);
		const binaryLen = binaryString.length;
		let bytes = new Uint8Array(binaryLen);
    for (let i = 0; i < binaryLen; i++) {
	      const ascii = binaryString.charCodeAt(i);
		    bytes[i] = ascii;
    }
	  return bytes;
}

export function saveByteArray(fileName, byte, type) {
		const blob = new Blob([byte], {type});
		let link = document.createElement('a');
		link.href = window.URL.createObjectURL(blob);
		link.download = fileName;
    link.click();
}
