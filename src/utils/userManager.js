import { createUserManager } from 'redux-oidc';

const userManagerConfig = {
    response_type: 'token id_token',
    scope: 'openid email profile',
    automaticSilentRenew: true,
    filterProtocolClaims: true,
    loadUserInfo: false,
    includeIdTokenInSilentRenew: true
}

userManagerConfig.client_id = "wJfzP8TpMXbWYNUy4Vhn59AGR63rHvdg";
userManagerConfig.redirect_uri = "https://glsupport-kb.translations.com/SSO";
userManagerConfig.authority = 'https://sso.transperfect.com';
userManagerConfig.silent_redirect_uri = "https://glsupport-kb.translations.com/silent-refresh.html";

const userManager = createUserManager(userManagerConfig);

export default userManager;