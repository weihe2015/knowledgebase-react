import React from 'react';
import ReactDOM from 'react-dom';
import decode from 'jwt-decode';
import thunk from 'redux-thunk';
import { BrowserRouter, Route } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { loadUser, OidcProvider } from "redux-oidc";
import logger from 'redux-logger';

import userManager from './utils/userManager';

import 'semantic-ui-css/semantic.min.css';

import App from './App';
import { userLoggedIn, logout } from './actions/auth';
import registerServiceWorker from './registerServiceWorker';
import rootReducer from './rootReducer';

import setAuthorizationHeader from './utils/setAuthorizationHeader';

const isLocalhost = Boolean(
	window.location.hostname === 'localhost' ||
	// [::1] is the IPv6 localhost address.
	window.location.hostname === '[::1]' ||
	// 127.0.0.1/8 is considered localhost for IPv4.
	window.location.hostname.match(
		/^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
	)
);

//const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk, logger)));
const store = (isLocalhost) ? createStore(rootReducer,
	composeWithDevTools(applyMiddleware(thunk, logger))
) : (createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk))));

loadUser(store, userManager);

if (localStorage.knowledgebaseJWT) {
	const payload = decode(localStorage.knowledgebaseJWT);
	const currentDate = new Date().getTime() / 1000;
	const expiredDate = payload.exp;
	if (expiredDate < currentDate) {
		logout();
	}
	else {
		const user = {
			token: localStorage.knowledgebaseJWT,
			email: payload.email,
			fullName: payload.fullName,
			confirmed: payload.confirmed,
			role: payload.role,
			products: payload.products
		};
		setAuthorizationHeader(localStorage.knowledgebaseJWT);
		store.dispatch(userLoggedIn(user));

	}
}

ReactDOM.render(
	<BrowserRouter>
		<Provider store={store}>
			<OidcProvider store={store} userManager={userManager}>
				<Route component={App} />
			</OidcProvider>
		</Provider>
	</BrowserRouter>,
	document.getElementById('root')
);

registerServiceWorker();
