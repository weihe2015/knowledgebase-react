import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import HomePage from './components/pages/HomePage';
import LoginPage from './components/pages/LoginPage';
import SignupPage from './components/pages/SignupPage';
import DashboardPage from './components/pages/DashboardPage';
import ConfirmationPage from './components/pages/ConfirmationPage';
import ForgetPasswordPage from './components/pages/ForgetPasswordPage';
import ResetPasswordPage from './components/pages/ResetPasswordPage';
import ChangePasswordPage from './components/pages/ChangePasswordPage';
import EditSolutionPage from './components/pages/EditSolutionPage';
import ViewSolutionPage from './components/pages/ViewSolutionPage';
import NoMatchPage from './components/pages/NoMatchPage';
import ModifyUserPage from './components/pages/ModifyUserPage';
import UserOAuthPage from './components/pages/UserOAuthPage';
import ModifyProductPage from './components/pages/ModifyProductPage';

import AllUserRoute from './components/routes/AllUserRoute';
import GuestRoute from './components/routes/GuestRoute';
import AdminRoute from './components/routes/AdminRoute';

const App = ({ location }) => (
  <Switch>
    <Route
      location={location}
      path="/"
      exact
      component={HomePage}
    />
    <Route
      location={location}
      path="/confirmation/:token"
      exact
      component={ConfirmationPage}
    />
    <Route
      location={location}
      path="/login"
      exact
      component={LoginPage}
    />
    <GuestRoute
      location={location}
      path="/forgetpassword"
      exact
      component={ForgetPasswordPage}
    />
    <GuestRoute
      location={location}
      path="/resetpassword/:token"
      exact
      component={ResetPasswordPage}
    />
    <AdminRoute
      location={location}
      path="/signup"
      exact
      component={SignupPage}
    />
    <AdminRoute
      location={location}
      path="/modifyuser"
      exact
      component={ModifyUserPage}
    />
    <AdminRoute
      location={location}
      path="/products"
      exact
      component={ModifyProductPage}
    />
    <AllUserRoute
      location={location}
      path="/dashboard"
      exact
      component={DashboardPage}
    />
    <AdminRoute
      location={location}
      path="/solutions/edit"
      exact
      component={EditSolutionPage}
    />
    <AllUserRoute
      location={location}
      path="/solutions/view"
      exact
      component={ViewSolutionPage}
    />
    <AllUserRoute
      location={location}
      path="/changePassword"
      exact
      component={ChangePasswordPage}
    />
    {/** OAuth page */}
    <Route 
      location={location}
      path="/SSO"
      exact
      component={UserOAuthPage}
    />
    <GuestRoute
      component={NoMatchPage}
    />
  </Switch>
);

App.propTypes = {
	location: PropTypes.shape({
		pathname: PropTypes.string.isRequired
	}).isRequired,
  isAuthenticated: PropTypes.bool.isRequired
}

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.email,
    isClient: state.user.role === 'client'
  }
}

export default connect(mapStateToProps)(App);
